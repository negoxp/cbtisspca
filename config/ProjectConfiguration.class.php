<?php

require_once dirname(__FILE__).'/../lib/vendor/symfony/lib/autoload/sfCoreAutoload.class.php';

require_once dirname(__FILE__).'/../lib/vendor/PHPExcel/PHPExcel.php';
require_once dirname(__FILE__).'/../lib/vendor/PHPExcel/PHPExcel/Writer/Excel2007.php';
require_once dirname(__FILE__).'/../lib/vendor/PHPExcel/PHPExcel/IOFactory.php'; 

$rendererName = PHPExcel_Settings::PDF_RENDERER_DOMPDF;
$rendererLibrary = 'dompdf.php';
$rendererLibraryPath = dirname(__FILE__). '/../lib/vendor/dompdf/' . $rendererLibrary;

//  Here's the magic: you __tell__ PHPExcel what rendering engine to use
//     and where the library is located in your filesystem
if (!PHPExcel_Settings::setPdfRenderer(
    $rendererName,
    $rendererLibraryPath
)) {
    die('NOTICE: Please set the $rendererName and $rendererLibraryPath values' .
        '<br />' .
        'at the top of this script as appropriate for your directory structure'
    );
}


//require_once dirname(__FILE__).'/../lib/vendor/PHPExcel/PHPExcel/Writer/PDF.php';  
//require_once dirname(__FILE__).'/../lib/vendor/PHPExcel/PHPExcel/Writer/PDF/DomPDF.php';   


sfCoreAutoload::register();  



class ProjectConfiguration extends sfProjectConfiguration
{
  public function setup()
  {
    $this->enablePlugins('sfPropelPlugin');
    $this->enablePlugins('sfGuardPlugin');
  }
}
