<?php
//@session_start();

header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
require_once(dirname(__FILE__).'/../config/ProjectConfiguration.class.php');

if (isset($_SERVER['ENV']) && ($_SERVER['ENV']=='local_jorge' || $_SERVER['ENV']=='local_fer')){
    $ambiente='dev';
    $debug=true;
}else{
    $ambiente="prod";
    $debug=true; 
}

//echo "1";

$configuration = ProjectConfiguration::getApplicationConfiguration('tutor', $ambiente, $debug);
sfContext::createInstance($configuration)->dispatch();
