var app = angular.module('myApp', []);

app.controller('myCtrl', function($scope, $http) {
 		
		// $scope will allow this to pass between controller and view
    $scope.formData = {};



    $scope.processForm = function() {
	  $http({
	  method  : 'POST',
	  url     : './check/process',
	  data    : $.param($scope.formData),  // pass in data as strings
	  headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
	 	}).then(function mySucces(response) {
	 			console.log(response);
	 			var data = response.data

				if (!data.success) {
		      // if not successful, bind errors to error variables
		      $scope.message = data.errors.name; 

		      $("#danger-alert").show("fast");
		      $("#alumno_detail").addClass( "jumbotron_red" );

		      $("#danger-alert").delay(3000).slideUp(200, function() {
					    $(this).hide();
					    $("#alumno_detail").removeClass( "jumbotron_red" );
					});
					$scope.formData.name = null;
		      $scope.errorName = "";

		      //$scope.errorSuperhero = data.errors.superheroAlias;
		    } else {
		      // if successful, bind success message to message
		      $("#listas").prepend('<li>'+data.alumno_nombre+' <strong>'+data.alumno_checkin+'</strong></li>');
		      $("#listas").slice(5,10).remove();

		      $scope.formData.name = null; 
		      $scope.errorName = "";

		      $scope.alumnoNombre = data.alumno_nombre;
		      $scope.alumnoNumeroControl = data.alumno_ncontrol;
		      $scope.alumnoFoto = "http://noabandono.cbtis19.mx/uploads/"+data.alumno_foto;
		      $scope.alumnoCheckin = data.alumno_checkin;

		      $scope.message = data.message;

		      

		      $("#success-alert").show("fast");
		      $("#alumno_detail").addClass( "jumbotron_green" );

		      $("#success-alert").delay(500).slideUp(200, function() {
					    $(this).hide();
					    $("#alumno_detail").removeClass( "jumbotron_green" );
					});

		    }

        //$scope.myWelcome = response.data;
    }, function myError(response) {
        $scope.message = response.statusText;
    });

	 };


});


$( document ).ready(function() {
  $( "#codebar_input" ).focus();

  $("#success-alert").hide();
  $("#danger-alert").hide();

  window.setInterval(function(){
  	$( "#codebar_input" ).focus();
	}, 50);

	setTimeout(function(){
   window.location.reload(1);
	}, 20000);


});

$( "#codebar_input" ).focus();



$("#codebar_input").on('keyup', function (e) {
    if (e.keyCode == 13) {
        // Do something
        //alert("hay enter");
    }
});

