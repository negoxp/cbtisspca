<?php

/**
 * package actions.
 *
 * @package    netsales
 * @subpackage package
 * @author     Jorge Flores
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class packageActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
      $this->getResponse()->setContentType('application/json');
    switch($request->getMethod()):
        case "POST":
            /*
            //Campos requeridos
            if(!$request->getParameter("name",false) || !trim($request->getParameter("name"))){
                return $this->renderText(json_encode(array('error'=>'Package name required')));
            }
            if(!$request->getParameter("restaurant",false) || !trim($request->getParameter("restaurant"))){
                return $this->renderText(json_encode(array('error'=>'Restaurant required')));
            }

            if(!$request->getParameter("content",false) || !trim($request->getParameter("content"))){
                return $this->renderText(json_encode(array('error'=>'Content required')));
            }

            //Buscamos el restaurant
            $tmp = mb_convert_case($request->getParameter("restaurant"),MB_CASE_UPPER);
            $key = str_replace("BK","",$tmp);
            if(!$restaurant = RestaurantPeer::retrieveByName($key)):
                return $this->renderText(json_encode(array('error'=>'Restaurant not found')));
            endif;

            //Verificamos por nombre si no esta repetido el paquete
            if($package = PackagePeer::retrieveByName($request->getParameter("name"))){
                return $this->renderText(json_encode(array('error'=>'Package name already exist')));
            }
            */

            $package = new Package();
            //$package->setName($request->getParameter("name"));
            //$package->setRestaurantId($restaurant->getId());
            $package->setContent($request->getParameter("content"));
            $package->setProcessed(0);
            $package->save();

            return $this->renderText(json_encode(array('success'=>'Package successfully processed')));
        default:
            return $this->renderText(json_encode(array('error'=>'Http method not found')));

        break;
    endswitch;
  }
}
