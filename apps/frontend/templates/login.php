<!DOCTYPE html>
<html lang="en">

<head>
      <meta content="width=device-width, initial-scale=1" name="viewport">
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico" />

      <!-- Bootstrap core CSS -->
      <link rel="stylesheet" href="<?php echo public_path("vendor/theme") ?>/css/bootstrap.css">
      <!-- Bootstrap theme -->
      <!--  <link rel="stylesheet" href="css/bootstrap-theme.min.css"> -->

      <!-- Custom styles for this template -->
      <link rel="stylesheet" href="<?php echo public_path("vendor/theme") ?>/css/login.css">
      <link rel="stylesheet" href="<?php echo public_path("vendor/theme") ?>/css/theme.css">
      <link rel="stylesheet" href="<?php echo public_path("vendor/theme") ?>/css/dashboard.css">
      <link rel="stylesheet" href="<?php echo public_path("vendor/theme") ?>/css/style.css">
      <link rel="stylesheet" href="<?php echo public_path("vendor/theme") ?>/css/dripicon.css">
      <link rel="stylesheet" href="<?php echo public_path("vendor/theme") ?>/css/typicons.css" />
      <link rel="stylesheet" href="<?php echo public_path("vendor/theme") ?>/css/font-awesome.css" />
      <link rel="stylesheet" href="<?php echo public_path("vendor/theme") ?>/css/responsive.css">
      <link rel="stylesheet" href="<?php echo public_path("vendor/theme") ?>/js/tip/tooltipster.css">
      <link rel="stylesheet" type="text/css" href="<?php echo public_path("vendor/theme") ?>/js/vegas/jquery.vegas.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo public_path("vendor/theme") ?>/js/number-progress-bar/number-pb.css">
      <!-- pace loader -->
      <script src="<?php echo public_path("vendor/theme") ?>/js/pace/pace.js"></script>
      <link href="<?php echo public_path("vendor/theme") ?>/js/pace/themes/orange/pace-theme-flash.css" rel="stylesheet" />



    <?php include_stylesheets() ?>


      <!-- Main jQuery Plugins -->
      <script type='text/javascript' src="<?php echo public_path("vendor/theme") ?>/js/jquery.js"></script>

    <?php include_javascripts() ?>
  </head>
  <body role="document">



  <div class="container-fluid ">
      <div class="row">
          <div class="col-md-4 col-md-offset-4">
              <!-- Comtainer -->
              <div class="paper-wrap bevel tlbr">
                  <div id="paper-top">
                      <div class="row">
                          <div class="col-lg-12 no-pad">
                              <!--     -->
                              <a class="navbar-brand logo-text" href="#">Bienvenidos</a>

                          </div>
                          <div class="col-lg-12">
                              <?php //echo image_tag("logo.png",array('class'=>'img-responsive')) ?>
                              <br />
                          </div>
                      </div>
                  </div>

                  <!-- CONTENT -->
                  <div style="min-height:400px;" class="wrap-fluid" id="paper-bg">

                      <div class="row">
                          <div class="col-lg-12">
                              <div class="account-box">

                                  <?php echo $sf_content ?>

                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <!-- / FOOTER -->
              <!-- Container -->
          </div>
      </div>
  </div>




  </body>
</html>
