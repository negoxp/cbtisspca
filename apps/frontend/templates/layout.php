<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<head>
      <meta content="width=device-width, initial-scale=1" name="viewport">
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico" />

      <!-- Bootstrap core CSS -->
      <link rel="stylesheet" href="<?php echo public_path("vendor/theme") ?>/css/bootstrap.css">
      <!-- Bootstrap theme -->
      <!--  <link rel="stylesheet" href="css/bootstrap-theme.min.css"> -->

      <!-- Custom styles for this template -->
      <link rel="stylesheet" href="<?php echo public_path("vendor/theme") ?>/css/theme.css">
      <link rel="stylesheet" href="<?php echo public_path("vendor/theme") ?>/css/dashboard.css">
      <link rel="stylesheet" href="<?php echo public_path("vendor/theme") ?>/css/style.css">
      <link rel="stylesheet" href="<?php echo public_path("vendor/theme") ?>/css/dripicon.css">
      <link rel="stylesheet" href="<?php echo public_path("vendor/theme") ?>/css/typicons.css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="<?php echo public_path("vendor/theme") ?>/css/responsive.css">
      <link rel="stylesheet" href="<?php echo public_path("vendor/theme") ?>/js/tip/tooltipster.css">
      <link rel="stylesheet" type="text/css" href="<?php echo public_path("vendor/theme") ?>/js/vegas/jquery.vegas.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo public_path("vendor/theme") ?>/js/number-progress-bar/number-pb.css">
      <!-- pace loader -->
      <script src="<?php echo public_path("vendor/theme") ?>/js/pace/pace.js"></script>
      <link href="<?php echo public_path("vendor/theme") ?>/js/pace/themes/orange/pace-theme-flash.css" rel="stylesheet" />


      <?php include_stylesheets() ?>


      <!-- Main jQuery Plugins -->
      <script type='text/javascript' src="<?php echo public_path("vendor/theme") ?>/js/jquery.js"></script>

    <?php include_javascripts() ?>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script> 
  </head>
  <body role="document">

  <div id="preloader">
      <div id="status">&nbsp;</div>
  </div>
<?php

 /* ?>
  <!-- TOPNAV -->

  <div class="row">
      <div class="col-lg-2">
          <?php //echo image_tag("logo.png",array('class'=>'img-responsive')) ?>
      </div>
      <div class="col-lg-3">
          <ul class="nav navbar-nav navbar-left list-unstyled list-inline text-amber date-list">
              <li><i class="fontello-th text-amber"></i>
              </li>
              <li id="Date"></li>
          </ul>
          <ul class="nav navbar-nav navbar-left list-unstyled list-inline text-amber date-list">
              <li><i class="fontello-stopwatch text-amber"></i>
              </li>
              <li id="hours"></li>
              <li class="point">:</li>
              <li id="min"></li>
              <li class="point">:</li>
              <li id="sec"></li>
          </ul>


      </div>
      <div class="col-lg-4">
          <!--
          <div style="margin-bottom:0;" class="alert text-white ">
              <button data-dismiss="alert" class="close" type="button">×</button>
              <span class="entypo-info-circled"></span>
              <strong>Wellcome back!</strong>&nbsp;&nbsp;Felipe, your last loggin was 1 day ago. Have a nice day
          </div>
        -->
      </div>

      <div class="col-lg-3">
          <ul class="nav navbar-nav navbar-right">
              <li>
                  <a data-toggle="dropdown" class="dropdown-toggle text-white" href="#">
                      <img alt="" class="admin-pic img-circle" src="<?php echo public_path('images/felipe.png'); ?>">Hola, Angel <b class="caret"></b>
                  </a>
                  <ul style="margin:25px 15px 0 0;" role="menu" class="dropdown-setting dropdown-menu bg-amber">
                      <li>
                          <a href="#">
                              <span class="entypo-user"></span>&nbsp;&nbsp;My Profile</a>
                      </li>
                      <li>
                          <a href="#">
                              <span class="entypo-vcard"></span>&nbsp;&nbsp;Account Setting</a>
                      </li>
                      <li>
                          <a href="#">
                              <span class="entypo-lifebuoy"></span>&nbsp;&nbsp;Help</a>
                      </li>

                      <li>
                          <a href="#">
                              <span class="entypo-basket"></span>&nbsp;&nbsp; Purchase</a>
                      </li>
                  </ul>
              </li>



          </ul>
      </div>

  </div>
  <!-- END OF TOPNAV -->


  <?php */ ?>
  <!-- Comtainer -->
  <div class="container-fluid paper-wrap bevel tlbr">
  <div id="paper-top">
  <div class="row">
  <div class="col-sm-3 no-pad">

      <a class="navbar-brand logo-text" href="#">SPCA CBTIS 19</a>
      <?php /* ?>
      <ul class="list-unstyled list-inline noft-btn">
          <li data-toggle="tooltip" data-placement="bottom" title="Profile"><i class=" icon-user"></i>
          </li>

          <li data-toggle="tooltip" data-placement="bottom" title="Log Out"> <a href="#" class="text-white"><i class="icon-upload"></i></a>
          </li>

      </ul>

      <?php */ ?>
  </div>
&nbsp;&nbsp;&nbsp;
  <div class="col-sm-6 no-pad">
    <?php /* ?>
      <ul style="margin-top:8px;" class="nav navbar-nav navbar-left list-unstyled list-inline text-gray date-list news-list">
          <!-- <li><i class="fontello-doc-text text-gray"></i>
          </li> -->
          <li>
              <ul class="list-unstyled top-newsticker text-gray news-list">
                  <li><i class="icon-graph-line text-gray"></i>&nbsp;&nbsp;
                      <strong>Sales,</strong>Lorem impsum &nbsp;<b>1,000 USD</b>
                  </li>
                  <li><i class="icon-graph-line  text-gray"></i>&nbsp;&nbsp;
                      <strong>Costs,</strong>Lorem impsum &nbsp;<b>1,000 USD</b>
                  </li>
                  <li><i class="icon-graph-line  text-gray"></i>&nbsp;&nbsp;
                      <strong>Inventory,</strong>Lorem impsum &nbsp;<b>1,000 USD</b>
                  </li>
              </ul>
          </li>
      </ul>
      <?php */ ?>

  </div>



  <div class="col-sm-3 no-pad">
  <!-- menu right -->
  <?php /* ?>
  <div class="navbar-right">
  <ul class="nav navbar-nav margin-left">
  <!-- Messages: style can be found in dropdown.less-->
  <li class="dropdown messages-menu">
      <div class="drop-btn dropdown-toggle bg-white" data-toggle="dropdown">
          <i class="fa  fa-envelope text-navy"></i>
          <span class="label label-success label-drop">4</span>
      </div>
      <ul class="dropdown-menu drop-msg ">
          <li class="header bg-green">
              You have 4 messages</li>

          <li>
              <!-- inner menu: contains the actual data -->
              <ul class="menu bg-white">
                  <li>
                      <!-- start message -->
                      <a href="#">
                          <div class="pull-left">
                              <img src="http://api.randomuser.me/portraits/thumb/men/37.jpg" class="img-circle" alt="User Image" />
                          </div>
                          <h4>
                              Developer
                              <!-- <small><i class="fa fa-clock-o"></i> 5 mins</small> -->
                          </h4>
                          <p>Bug fixed level 90%</p>
                      </a>
                  </li>
                  <!-- end message -->
                  <li>
                      <a href="#">
                          <div class="pull-left">
                              <img src="http://api.randomuser.me/portraits/thumb/women/36.jpg" class="img-circle" alt="user image" />
                          </div>
                          <h4>
                              Aplication Support

                          </h4>
                          <p>There is some bug in your last submit</p>
                      </a>
                  </li>
                  <li>
                      <a href="#">
                          <div class="pull-left">
                              <img src="http://api.randomuser.me/portraits/thumb/men/35.jpg" class="img-circle" alt="user image" />
                          </div>
                          <h4>
                              Lead Developers

                          </h4>
                          <p>Please check again your submit</p>
                      </a>
                  </li>
                  <li>
                      <a href="#">
                          <div class="pull-left">
                              <img src="http://api.randomuser.me/portraits/thumb/women/34.jpg" class="img-circle" alt="user image" />
                          </div>
                          <h4>
                              Web Designer

                          </h4>
                          <p>Art has done</p>
                      </a>
                  </li>
                  <li>
                      <a href="#">
                          <div class="pull-left">
                              <img src="http://api.randomuser.me/portraits/thumb/men/33.jpg" class="img-circle" alt="user image" />
                          </div>
                          <h4>
                              General Manager

                          </h4>
                          <p>Employed newslatter</p>
                      </a>
                  </li>
              </ul>
          </li>

          <li class="footer-green">
              <!--    <div class="btn btn-xs bg-opacity-white-btn  fontello-arrows-cw"></div>
              <div class="btn btn-xs bg-opacity-white-btn fontello-trash"></div>
              <div class="btn btn-xs bg-opacity-white-btn fontello-eye-outline"></div> -->
          </li>
      </ul>
  </li>
  <!-- Notifications: style can be found in dropdown.less -->
  <li class="dropdown notifications-menu">
      <div class="drop-btn dropdown-toggle bg-white" data-toggle="dropdown">
          <i class="fa  fa-exclamation-triangle text-navy"></i>
          <span class="label bg-aqua label-drop">7</span>
      </div>
      <ul class="dropdown-menu drop-noft">

          <li class="header bg-aqua">
              You have 10 notifications</li>

          <li>
              <!-- inner menu: contains the actual data -->
              <ul class="menu bg-white">
                  <li>
                      <a href="#">
                          <i class="fa icon-user"></i> New developer registered
                      </a>
                  </li>
                  <li>
                      <a href="#">
                          <i class="fa icon-cloud"></i> 2 item commit
                      </a>
                  </li>
                  <li>
                      <a href="#">
                          <i class="fa icon-download"></i> 3 members joined
                      </a>
                  </li>

                  <li>
                      <a href="#">
                          <i class="fa icon-tag"></i> 22 sales made
                      </a>
                  </li>
                  <li>
                      <a href="#">
                          <i class="fa icon-document"></i> New task from manager
                      </a>
                  </li>
              </ul>
          </li>

          <li class="footer-blue">

          </li>
      </ul>
  </li>
  <!-- Tasks: style can be found in dropdown.less -->
  <li class="dropdown tasks-menu">
      <div class="drop-btn bg-white dropdown-toggle" data-toggle="dropdown">
          <i class="fa fa-briefcase text-navy"></i>
          <span class="label bg-red label-drop">9</span>
      </div>
      <ul class="dropdown-menu drop-task">

          <li class="header bg-red">
              <span></span>You have 9 tasks</li>

          <li>
              <!-- inner menu: contains the actual data -->
              <ul class="menu bg-white">
                  <li>
                      <!-- Task item -->
                      <div class="task-list-item">
                          <h2>Wed, 25 Mar 2014
                                                        <span>9:32
                                                            <small>PM</small>
                                                        </span>
                          </h2>
                          <h1>Finished task Testing.</h1>
                          <p>Lorem ipsum dollor si amet amet jabang bayi</p>
                      </div>
                  </li>
                  <!-- end task item -->
                  <li>
                      <!-- Task item -->

                      <div class="task-list-item">
                          <h2>Thu, 23 Mar 2014
                                                        <span>7:54
                                                            <small>PM</small>
                                                        </span>
                          </h2>
                          <h1>Creat the documentation</h1>
                          <p>Lorem ipsum dollor si amet amet jabang bayi</p>
                      </div>

                  </li>
                  <!-- end task item -->
                  <li>
                      <!-- Task item -->
                      <div class="task-list-item">
                          <h2>Wed, 21 Mar 2014
                                                        <span>12:43
                                                            <small>PM</small>
                                                        </span>
                          </h2>
                          <h1>Repository you file now!</h1>
                          <p>Lorem ipsum dollor si amet amet jabang bayi</p>
                      </div>
                  </li>
                  <!-- end task item -->
                  <li>
                      <!-- Task item -->
                      <div class="task-list-item">
                          <h2>Fri, 20 Mar 2014
                                                        <span>8:00
                                                            <small>PM</small>
                                                        </span>
                          </h2>
                          <h1>Fill the job description</h1>
                          <p>Lorem ipsum dollor si amet amet jabang bayi</p>
                      </div>
                  </li>
                  <!-- end task item -->
              </ul>
          </li>

          <li class="footer-red">

          </li>
      </ul>
  </li>


  </ul>
  </div>
  </div>
  <!-- end of menu right -->
    <?php */ ?>
  </div>
  </div>



  <!-- SIDE MENU -->
  <div class="wrap-sidebar-content">
  <div id="skin-select">
  <a id="toggle">
      <span class="fa icon-menu"></span>
  </a>

  <div class="skin-part">
  <div id="tree-wrap">
  <div class="side-bar">
  <ul id="menu-showhide" class="topnav">
  <li class="devider-title">
      <h3>
          <span>Menu</span>
      </h3>
  </li>

<?php

/*

      <li>
          <a class="tooltip-tip" href="#" title="Today Operations">
              <i class=" fontello-stopwatch"></i>
              <span>today operations</span>

          </a>
          <ul>
              <!--  <li class="hide-min-toggle">UI Element</li> -->
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Index">Today Operations</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Compose">Today Service</a>
              </li>
          </ul>
      </li>




      <li>
          <a class="tooltip-tip" href="#" title="Daily Operations">
              <i class=" fontello-calendar-1"></i>
              <span>Daily Operations</span>

          </a>
          <ul>
              <!--  <li class="hide-min-toggle">UI Element</li> -->
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Sales Summary">Sales Summary</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Service Performance">Service Performance</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Product Mix">Product Mix</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Top Items Summary">Top Items Summary</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Adjustments">Adjustments</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Discounts">Discounts</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Sales Mix Summary">Sales Mix Summary</a>
              </li>


          </ul>
      </li>


      <li>
          <a class="tooltip-tip" href="#" title="Daily Performance">
              <i class=" fontello-cog-1"></i>
              <span>Daily Performance</span>

          </a>
          <ul>
              <!--  <li class="hide-min-toggle">UI Element</li> -->
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Cost of Goods">Cost of Goods</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Daypart Exceptions">Daypart Exceptions</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Daypart Productivity">Daypart Productivity</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Employee Performance">Employee Performance</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Employee Sales Summary">Employee Sales Summary</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Employee Control Report">Employee Control Report</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Compose">Employee Item Sales</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Weather Analysis">Weather Analysis</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Employee Exceptions">Employee Exceptions</a>
              </li>

          </ul>
      </li>




      <li>
          <a class="tooltip-tip" href="#" title="Comparison Reports">
              <i class=" fa fa-fw fa-table"></i>
              <span>Comparison Reports</span>

          </a>
          <ul>
              <!--  <li class="hide-min-toggle">UI Element</li> -->
              <li>
                  <!-- class="active" -->
                  <a href="#" title="KPIs by Date">KPIs by Date</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Ops. Comparison by Locations">Ops. Comparison by Locations</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="KPIs by Store">KPIs by Store</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Ops. Comparison by Date">Ops. Comparison by Date</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Sales Comparison per Day">Sales Comparison per Day</a>
              </li>


          </ul>
      </li>




      <li>
          <a class="tooltip-tip" href="#" title="Labor Cost">
              <i class=" fontello-money"></i>
              <span>Labor Cost</span>

          </a>
          <ul>
              <!--  <li class="hide-min-toggle">UI Element</li> -->
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Labor Analysis">Labor Analysis</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Job Code Summary">Job Code Summary</a>
              </li>
          </ul>
      </li>





      <li>
          <a class="tooltip-tip" href="#" title="Ad-hoc Reports">
              <i class="fa fa-bar-chart-o"></i>
              <span>Ad-hoc Reports</span>

          </a>
          <ul>
              <!--  <li class="hide-min-toggle">UI Element</li> -->
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Small Beverages">Small Beverages</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Medium Beverages">Medium Beverages</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Large Beverages">Large Beverages</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Small Beverages Sum">Small Beverages Sum</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Medium Beverages Sum">Medium Beverages Sum</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Large Beverages Sum">Large Beverages Sum</a>
              </li>

          </ul>
      </li>





      <li>
          <a class="tooltip-tip" href="#" title="HR Reports">
              <i class="fa fa-bar-chart-o"></i>
              <span>HR Reports</span>

          </a>
          <ul>
              <!--  <li class="hide-min-toggle">UI Element</li> -->
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Payroll Summary">Payroll Summary</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Payroll Detail">Payroll Detail</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Time Card Summary">Time Card Summary</a>
              </li>
              <li>
                  <!-- class="active" -->
                  <a href="#" title="Payroll by Store">Payroll by Store</a>
              </li>

          </ul>
      </li>





  <li>
      <a class="tooltip-tip" href="#" title="Dashboards">
          <i class="fontello-params"></i>
          <span>Dashboards</span>

      </a>
      <ul>
          <!--  <li class="hide-min-toggle">UI Element</li> -->
          <li>
              <!-- class="active" -->
              <a href="#" title="Sales and Cost Dashboard">Sales and Cost Dashboard</a>
          </li>
          <li>
              <!-- class="active" -->
              <a href="#" title="Service Perf Dashboard">Service Perf Dashboard</a>
          </li>
          <li>
              <!-- class="active" -->
              <a href="#" title="Operations Dashboard">Operations Dashboard</a>
          </li>


      </ul>
  </li>
*/
  ?>



<!--
  
  <li >
      <a style="border-left:4px solid #5F9BDB; padding:0 0 0 16px;" class="tooltip-tip" href="#" title="Logout">
          <i class="fa fa-bars"></i>
          <span>Sms Log</span>
      </a>
      <ul>
        <li><a href="<?php echo url_for("home/log?filter=all") ?>" >Todos</a></li>
        <li><a href="<?php echo url_for("home/log?filter=col") ?>" >Colima</a></li>
        <li><a href="<?php echo url_for("home/log?filter=jal") ?>" >Jalisco</a></li>
        <li><a href="<?php echo url_for("home/log?filter=gto") ?>" >Gto</a></li>
        <li><a href="<?php echo url_for("home/log?filter=mich") ?>" >Mich</a></li>
        <li><a href="<?php echo url_for("home/log?filter=gdl") ?>" >gdl</a></li>
        <li></li>
        <li><a href="<?php echo url_for("home/log?hablar=1") ?>" ><i class="fa fa-phone"></i> Hablar</a></li>
        <li><a href="<?php echo url_for("home/log?borrados=1") ?>" ><i class="fa fa-times"></i> Borrados</a></li>
        <li><a href="<?php echo url_for("home/log?chkerror=1") ?>" ><i class="fa fa-circle text-danger"></i> Check error</a></li>
        <li><a href="<?php echo url_for("home/log?procesados=1") ?>" ><i class="fa fa-check text-success"></i> Procesados</a></li>
        <li><a href="<?php echo url_for("home/log?errorlog=1") ?>" ><i class="fa fa-times text-warning"></i> Error log</a></li>
      </ul>
  </li>
-->
  <li >
      <a style="border-left:4px solid #5F9BDB; padding:0 0 0 16px;" class="tooltip-tip" href="<?php echo url_for("alumno/index") ?>" title="Logout">
          <i class="fa fa-users"></i>
          <span>Alumnos</span>
      </a>
  </li>
  <li >
      <a style="border-left:4px solid #5F9BDB; padding:0 0 0 16px;" class="tooltip-tip" href="<?php echo url_for("reporte/index") ?>" title="Logout">
          <i class="fa fa-clone"></i>
          <span>Reportes</span>
      </a>
  </li>
  <?php if($sf_user->getGuardUser()->getIsSuperAdmin()): ?>
  <li >
      <a style="border-left:4px solid #5F9BDB; padding:0 0 0 16px;" class="tooltip-tip" href="#" title="">
          <i class="fa fa-cog"></i>
          <span>Configuracion</span>
      </a>
      <ul>
        <li><a href="<?php echo url_for("nivel/index") ?>" >Niveles</a></li>
      </ul>
  </li>
  <li >
      <a style="border-left:4px solid #5F9BDB; padding:0 0 0 16px;" class="tooltip-tip" href="<?php echo url_for("@sf_guard_user") ?>" title="Logout">
          <i class="fa fa-user"></i>
          <span>Users</span>

      </a>
  </li>
  <?php endif; ?>

  <li >
      <a style="border-left:4px solid #5F9BDB; padding:0 0 0 16px;" class="tooltip-tip" href="<?php echo url_for("check/index") ?>" title="Logout">
          <i class="fa fa-clock-o"></i>
          <span>Checador</span>
      </a>
  </li>


  <li >
      <a style="border-left:4px solid #5F9BDB; padding:0 0 0 16px;" class="tooltip-tip" href="<?php echo url_for("@sf_guard_signout") ?>" title="Logout">
          <i class="fa fa-power-off"></i>
          <span>Salir</span>
      </a>
  </li>

  </ul>
  <div class="side-dash">

      <ul class="side-dashh-list">

      </ul>


      <div id="doughnutChart" class="chart"></div>
  </div>
  </div>
  </div>
  </div>
  </div>
  <!-- #/skin-select -->
  <!-- END OF SIDE MENU -->

  <!-- Breadcrumb -->
  <div class="sub-board">
                <span class="header-icon"><i class="fontello-home"></i>
                </span>
      <ol class="breadcrumb newcrumb ng-scope">
          <li>
              <a href="#">
                            <span>
                            </span>dashboard</a>
          </li>

      </ol>


      <div class="dark" style="visibility: visible;">
          <!--
          <form class="navbar-form navbar-left" role="search">
              <div class="form-group">
                  <input type="text" class="form-control search rounded id_search" placeholder="Search">
              </div>
          </form>
        -->
      </div>
  </div>
  <!-- End of Breadcrumb -->






  <!-- CONTENT -->
  <div class="wrap-fluid" id="paper-bg">
    <div class="tab-content">
    <?php if ($sf_request->getParameter('error')!=""){?>
    <?php if ($sf_request->getParameter('error')=="1"){?> 
    <div class="alert alert-danger" role="alert">
      <strong>Mensaje con ERROR!</strong> <?php echo $sf_request->getParameter('errorlog'); ?>
    </div>
    <?php }else{?>

    <div class="alert alert-success" role="alert">
      <strong>Mensaje Procesado!</strong> <?php echo $sf_request->getParameter('errorlog'); ?>
    </div>
    <?php 
    }
    }?>

    

      <?php echo $sf_content ?>
    </div>
  </div>
  <!-- #/paper bg -->
  </div>
  <!-- ./wrap-sidebar-content -->

  <!-- / END OF CONTENT -->

  <!-- FOOTER -->
  <div id="footer">
      <div class="devider-footer-left"></div>
      <div class="time">
          <p id="spanDate"></p>
          <p id="clock"></p>
      </div>
      <div class="copyright">Copyright &copy; 2015 - Proyecta
      </div>
      <div class="devider-footer"></div>
      <ul>
          <li><i class="fa fa-facebook-square"></i>
          </li>
          <li><i class="fa fa-twitter-square"></i>
          </li>
          <li><i class="fa fa-instagram"></i>
          </li>
      </ul>
  </div>
  <!-- / FOOTER -->
  </div>
  <!-- Container -->










    <script type='text/javascript' src='<?php echo public_path("vendor/theme") ?>/js/bootstrap.js'></script>
    <script type='text/javascript' src='<?php echo public_path("vendor/theme") ?>/js/date.js'></script>
    <script type='text/javascript' src='<?php echo public_path("vendor/theme") ?>/js/slimscroll/jquery.slimscroll.js'></script>
    <script type='text/javascript' src='<?php echo public_path("vendor/theme") ?>/js/jquery.nicescroll.min.js'></script>
    <script type='text/javascript' src='<?php echo public_path("vendor/theme") ?>/js/sliding-menu.js'></script>
    <script type='text/javascript' src='<?php echo public_path("vendor/theme") ?>/js/scriptbreaker-multiple-accordion-1.js'></script>
    <script type='text/javascript' src='<?php echo public_path("vendor/theme") ?>/js/tip/jquery.tooltipster.min.js'></script>
    <script type='text/javascript' src="<?php echo public_path("vendor/theme") ?>/js/donut-chart/jquery.drawDoughnutChart.js"></script>
    <script type='text/javascript' src="<?php echo public_path("vendor/theme") ?>/js/tab/jquery.newsTicker.js"></script>
    <script type='text/javascript' src="<?php echo public_path("vendor/theme") ?>/js/tab/app.ticker.js"></script>
    <script type='text/javascript' src='<?php echo public_path("vendor/theme") ?>/js/app.js'></script>


    <script type='text/javascript' src='<?php echo public_path("vendor/theme") ?>/js/vegas/jquery.vegas.js'></script>
    <script type='text/javascript' src='<?php echo public_path("vendor/theme") ?>/js/image-background.js'></script>
    <script type="text/javascript" src="<?php echo public_path("vendor/theme") ?>/js/jquery.tabSlideOut.v1.3.js"></script>
    <script type="text/javascript" src="<?php echo public_path("vendor/theme") ?>/js/bg-changer.js"></script>

    <script type='text/javascript' src="<?php echo public_path("vendor/theme") ?>/js/number-progress-bar/jquery.velocity.min.js"></script>
    <script type='text/javascript' src="<?php echo public_path("vendor/theme") ?>/js/number-progress-bar/number-pb.js"></script>
    <script src="<?php echo public_path("vendor/theme") ?>/js/loader/loader.js" type="text/javascript"></script>
    <script src="<?php echo public_path("vendor/theme") ?>/js/loader/demo.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo public_path("vendor/theme") ?>/js/skycons/skycons.js"></script>

    <!-- FLOT CHARTS -->
    <script src="<?php echo public_path("vendor/theme") ?>/js/flot/jquery.flot.min.js" type="text/javascript"></script>
    <!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
    <script src="<?php echo public_path("vendor/theme") ?>/js/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
    <!-- FLOT PIE PLUGIN - also used to draw donut charts -->
    <script src="<?php echo public_path("vendor/theme") ?>/js/flot/jquery.flot.pie.min.js" type="text/javascript"></script>
    <!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
    <script src="<?php echo public_path("vendor/theme") ?>/js/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
    <!-- Page script -->


  </body>
</html>
