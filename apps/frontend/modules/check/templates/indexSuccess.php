<h1>Checks List</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Alumno</th>
      <th>Tiempo</th>
      <th>Tipo</th>
      <th>Dia semana</th>
      <th>Dia</th>
      <th>Semana</th>
      <th>Mes</th>
      <th>Ano</th>
      <th>Hora</th>
      <th>Updated at</th>
      <th>Created at</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($checks as $check): ?>
    <tr>
      <td><a href="<?php echo url_for('check/edit?id='.$check->getId()) ?>"><?php echo $check->getId() ?></a></td>
      <td><?php echo $check->getAlumnoId() ?></td>
      <td><?php echo $check->getTiempo() ?></td>
      <td><?php echo $check->getTipo() ?></td>
      <td><?php echo $check->getDiaSemana() ?></td>
      <td><?php echo $check->getDia() ?></td>
      <td><?php echo $check->getSemana() ?></td>
      <td><?php echo $check->getMes() ?></td>
      <td><?php echo $check->getAno() ?></td>
      <td><?php echo $check->getHora() ?></td>
      <td><?php echo $check->getUpdatedAt() ?></td>
      <td><?php echo $check->getCreatedAt() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

  <a href="<?php echo url_for('check/new') ?>">New</a>
