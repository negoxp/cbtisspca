<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<form action="<?php echo url_for('check/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          <?php echo $form->renderHiddenFields(false) ?>
          &nbsp;<a href="<?php echo url_for('check/index') ?>">Back to list</a>
          <?php if (!$form->getObject()->isNew()): ?>
            &nbsp;<?php echo link_to('Delete', 'check/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
          <?php endif; ?>
          <input type="submit" value="Save" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><?php echo $form['alumno_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['alumno_id']->renderError() ?>
          <?php echo $form['alumno_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['tiempo']->renderLabel() ?></th>
        <td>
          <?php echo $form['tiempo']->renderError() ?>
          <?php echo $form['tiempo'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['tipo']->renderLabel() ?></th>
        <td>
          <?php echo $form['tipo']->renderError() ?>
          <?php echo $form['tipo'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['dia_semana']->renderLabel() ?></th>
        <td>
          <?php echo $form['dia_semana']->renderError() ?>
          <?php echo $form['dia_semana'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['dia']->renderLabel() ?></th>
        <td>
          <?php echo $form['dia']->renderError() ?>
          <?php echo $form['dia'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['semana']->renderLabel() ?></th>
        <td>
          <?php echo $form['semana']->renderError() ?>
          <?php echo $form['semana'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['mes']->renderLabel() ?></th>
        <td>
          <?php echo $form['mes']->renderError() ?>
          <?php echo $form['mes'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['ano']->renderLabel() ?></th>
        <td>
          <?php echo $form['ano']->renderError() ?>
          <?php echo $form['ano'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['hora']->renderLabel() ?></th>
        <td>
          <?php echo $form['hora']->renderError() ?>
          <?php echo $form['hora'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['updated_at']->renderLabel() ?></th>
        <td>
          <?php echo $form['updated_at']->renderError() ?>
          <?php echo $form['updated_at'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['created_at']->renderLabel() ?></th>
        <td>
          <?php echo $form['created_at']->renderError() ?>
          <?php echo $form['created_at'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
