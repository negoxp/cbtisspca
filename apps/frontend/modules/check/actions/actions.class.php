<?php

/**
 * check actions.
 *
 * @package    netsales
 * @subpackage check
 * @author     Jorge Flores
 */
class checkActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    
    $this->checks = CheckinPeer::doSelect(new Criteria());
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new checkinForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new checkinForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($check = CheckinPeer::retrieveByPk($request->getParameter('id')), sprintf('Object check does not exist (%s).', $request->getParameter('id')));
    $this->form = new checkinForm($check);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($check = CheckinPeer::retrieveByPk($request->getParameter('id')), sprintf('Object check does not exist (%s).', $request->getParameter('id')));
    $this->form = new checkinForm($check);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($check = CheckinPeer::retrieveByPk($request->getParameter('id')), sprintf('Object check does not exist (%s).', $request->getParameter('id')));
    $check->delete();

    $this->redirect('check/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $check = $form->save();

      $this->redirect('check/edit?id='.$check->getId());
    }
  }
}
