<?php

/**
 * alumno actions.
 *
 * @package    netsales
 * @subpackage alumno
 * @author     Jorge Flores
 */
class alumnoActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $this->alumnos = AlumnoPeer::doSelect(new Criteria());
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new alumnoForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new alumnoForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($alumno = AlumnoPeer::retrieveByPk($request->getParameter('id')), sprintf('Object alumno does not exist (%s).', $request->getParameter('id')));
    $this->form = new alumnoForm($alumno);
    $this->alumno=$alumno;

    $user = $this->getUser(); 

    $c=new Criteria();
    if (!$user->isSuperAdmin()){
      //$c->addand(ReportePeer::USER_ID,$user->getProfile()->getUserId(), Criteria::EQUAL);
    }

    $c->addand(ReportePeer::ALUMNO_ID,$alumno->getId(), Criteria::EQUAL);

    $this->reportes=ReportePeer::doSelect($c);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($alumno = AlumnoPeer::retrieveByPk($request->getParameter('id')), sprintf('Object alumno does not exist (%s).', $request->getParameter('id')));
    $this->form = new alumnoForm($alumno);


    $this->processForm($request, $this->form);

    $this->alumno=$alumno;
    $user = $this->getUser();

    $c=new Criteria();
    if (!$user->isSuperAdmin()){
      $c->addand(ReportePeer::USER_ID,$user->getProfile()->getUserId(), Criteria::EQUAL);
    }

    $c->addand(ReportePeer::ALUMNO_ID,$alumno->getId(), Criteria::EQUAL);

    $this->reportes=ReportePeer::doSelect($c);
    
    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($alumno = AlumnoPeer::retrieveByPk($request->getParameter('id')), sprintf('Object alumno does not exist (%s).', $request->getParameter('id')));
    $alumno->delete();

    $this->redirect('alumno/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {

      $alumno = $form->save();
      $this->redirect('alumno/edit?id='.$alumno->getId());
    }
  }
}
