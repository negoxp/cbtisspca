<h1>Alumnos </h1>

<table class="table datatable">
  <thead>
    <tr>
      <th>Ncontrol</th>
      <th>Foto Cred.</th>
      <th>Nombre</th>
      <th>Paterno</th>
      <th>Materno</th>

      <th>Carrera</th>
      <th>Reportes</th>
      <!--<th>Generacion</th>-->
      <!--<th>Turno</th>-->
      <th>Semestre</th>
      <th>Grupo</th>
      <th></th>

    </tr>
  </thead>
  <tbody>
    <?php foreach ($alumnos as $alumno): ?>
    <tr>
      <td><?php echo $alumno->getNcontrol() ?></td>
      <!--<td><?php echo $alumno->getNreportes() ?></td>-->
      <td><?php if ($alumno->getFoto()!="") { echo "Si";} ?></td>
      <td><?php echo $alumno->getNombre() ?></td>
      <td><?php echo $alumno->getPaterno() ?></td>
      <td><?php echo $alumno->getMaterno() ?></td>

      <td><?php echo $alumno->getCarrera() ?></td>
      <td><?php echo $alumno->getNreportes() ?></td>
      <!--<td><?php echo $alumno->getGeneracion() ?></td>-->
      <!--<td><?php echo $alumno->getTurno() ?></td>-->
      <td><?php echo $alumno->getSemestre() ?></td>
      <td><?php echo $alumno->getGrupo() ?></td>
      <td><a  class="btn btn-default btn-sm" href="<?php echo url_for('alumno/edit?id='.$alumno->getId()) ?>"><i class="fa fa-pencil"></i></a></td>

    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

  <a href="<?php echo url_for('alumno/new') ?>">New</a>



<script type="text/javascript">
$(document).ready( function () {
    $('.datatable').dataTable({
      "sDom": 'T<"clear">lfrtip',
      aLengthMenu: [
        [10, 25, 100, -1],
        [10, 25, 100, "Todos"]
      ],
      iDisplayLength: 25,
      "oLanguage": {
        "sSearch": "Buscar: "
      }
    });


} );
</script>