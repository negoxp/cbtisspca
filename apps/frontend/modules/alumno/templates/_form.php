<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<form action="<?php echo url_for('alumno/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>

 
 <div class="row">
  <div class="col-md-10">
    <h3><?php echo $alumno->getNcontrol()." - ".$alumno->getNombre()." ".$alumno->getPaterno()." ".$alumno->getMaterno(); ?></h3>
  </div>
  <div class="col-md-2">
    <a  class="btn btn-warning btn-sm" href="<?php echo url_for('reporte/new?alumno_id='.$alumno->getId()) ?>"><i class="fa fa-plus"></i> Agregar Reporte</a>
  </div>
</div>

  <table class="table">
    <tfoot>
      <tr>
        <td colspan="2">
          <!--
          <?php echo $form->renderHiddenFields(false) ?>
          &nbsp;<a href="<?php echo url_for('alumno/index') ?>">Back to list</a>
          <?php if (!$form->getObject()->isNew()): ?>
            &nbsp;<?php echo link_to('Delete', 'alumno/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
          <?php endif; ?>
          -->
          <input type="submit" value="Guardar" class="btn btn-default" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr class="hide">
        <th><?php echo $form['ncontrol']->renderLabel() ?></th>
        <td>
          <?php echo $form['ncontrol']->renderError() ?>
          <?php echo $form['ncontrol'] ?>
          <?php echo $form['nombre'] ?>
          <?php echo $form['paterno'] ?>
          <?php echo $form['materno'] ?>
        </td>
      </tr>
      <tr>
        <td>
          <?php if(is_file(sfConfig::get("sf_upload_dir").DIRECTORY_SEPARATOR.$alumno->getFoto())){ ?> 
          <img src="<?php echo public_path("uploads/".$alumno->getFoto(),1) ?>" width="150" /></td>
          <?php } ?>
        </td>
        <td>
          <h3>
          Control: <?php echo $alumno->getNcontrol(); ?>
          <br/>
          Nombre: <?php echo $alumno->getNombre()." ".$alumno->getPaterno()." ".$alumno->getMaterno(); ?>
        </h3>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['centro']->renderLabel() ?></th>
        <td>
          <?php echo $form['centro']->renderError() ?>
          <?php echo $form['centro'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['plantel']->renderLabel() ?></th>
        <td>
          <?php echo $form['plantel']->renderError() ?>
          <?php echo $form['plantel'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['carrera']->renderLabel() ?></th>
        <td>
          <?php echo $form['carrera']->renderError() ?>
          <?php echo $form['carrera'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['generacion']->renderLabel() ?></th>
        <td>
          <?php echo $form['generacion']->renderError() ?>
          <?php echo $form['generacion'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['turno']->renderLabel() ?></th>
        <td>
          <?php echo $form['turno']->renderError() ?>
          <?php echo $form['turno'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['semestre']->renderLabel() ?></th>
        <td>
          <?php echo $form['semestre']->renderError() ?>
          <?php echo $form['semestre'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['grupo']->renderLabel() ?></th>
        <td>
          <?php echo $form['grupo']->renderError() ?>
          <?php echo $form['grupo'] ?>
        </td>
      </tr>
      <!--
      <tr>
        <th><?php echo $form['ncontrol']->renderLabel() ?></th>
        <td>
          <?php echo $form['ncontrol']->renderError() ?>
          <?php echo $form['ncontrol'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['nombre']->renderLabel() ?></th>
        <td>
          <?php echo $form['nombre']->renderError() ?>
          <?php echo $form['nombre'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['paterno']->renderLabel() ?></th>
        <td>
          <?php echo $form['paterno']->renderError() ?>
          <?php echo $form['paterno'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['materno']->renderLabel() ?></th>
        <td>
          <?php echo $form['materno']->renderError() ?>
          <?php echo $form['materno'] ?>
        </td>
      </tr>
      -->
      <tr>
        <th><?php echo $form['curp']->renderLabel() ?></th>
        <td>
          <?php echo $form['curp']->renderError() ?>
          <?php echo $form['curp'] ?>
        </td>
      </tr>

      <tr>
        <th><?php echo $form['foto']->renderLabel() ?></th>
        <td>
          <?php echo $form['foto']->renderError() ?>
          <?php echo $form['foto'] ?>
        </td>
      </tr>
  <!--    
      <tr>
        <th><?php echo $form['created_at']->renderLabel() ?></th>
        <td>
          <?php echo $form['created_at']->renderError() ?>
          <?php echo $form['created_at'] ?>
        </td>
      </tr>
    -->
    </tbody>
  </table>
</form>


