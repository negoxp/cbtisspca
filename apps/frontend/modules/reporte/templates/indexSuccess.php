<h1>Reportes</h1>

<table class="table">
  <thead>
    <tr>
      <th>Id</th>
      <th>Alumno</th>
      <th>Reporte</th>
      <th>Nivel</th>
      <th>Usuario</th>
      <th>Fecha</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($reportes as $reporte): ?>
    <tr>
      <td><a href="<?php echo url_for('reporte/edit?id='.$reporte->getId()) ?>"><?php echo $reporte->getId() ?></a></td>
      <td><?php echo $reporte->getAlumno()->getNombreCompleto() ?></td>
      <td><?php echo $reporte->getReporte() ?></td>
      <td><?php echo $reporte->getNivel()->getNivel() ?><i class="fa fa-circle" style="color:<?php echo colort($reporte->getNivel()->getNivel()); ?>;"></td>
      <td><?php echo $reporte->getSfGuardUser()->getUsername() ?></td>
      <td><?php echo date("d-m-Y",strtotime($reporte->getCreatedAt())) ?></td>
      <td><a  class="btn btn-default btn-sm" href="<?php echo url_for('reporte/edit?id='.$reporte->getId()) ?>"><i class="fa fa-pencil"></i></a><td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<!--
  <a href="<?php echo url_for('reporte/new') ?>">New</a>
-->