<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<form action="<?php echo url_for('reporte/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <table class="table">
    <tfoot>
      <tr>
        <td colspan="2">
          <?php echo $form->renderHiddenFields(false) ?>
          &nbsp;<a href="<?php echo url_for('reporte/index') ?>" class="btn btn-default">Regresar</a>

          <?php if($sf_user->getGuardUser()->getIsSuperAdmin() or $sf_user->getGuardUser()->getId()==$form->getObject()->getUserId()): ?>
            <?php if (!$form->getObject()->isNew()){ ?>
              &nbsp;<?php echo link_to('Eliminar', 'reporte/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => 'Estas seguro de eliminar?', 'class' => 'btn btn-danger')) ?>
              <input type="submit" value="Guardar" class="btn btn-success" />
            <?php }else{ ?>
              <input type="submit" value="Guardar" class="btn btn-success" />
            <?php } ?>

          <?php endif; ?>

        </td> 
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><?php echo $form['alumno_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['alumno_id']->renderError() ?>
          <?php echo $form['alumno_id'] ?>
        </td>
      </tr>
      <tr class="hide">
        <th><?php echo $form['user_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['user_id']->renderError() ?>
          <?php echo $form['user_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['reporte']->renderLabel() ?></th>
        <td>
          <?php echo $form['reporte']->renderError() ?>
          <?php echo $form['reporte'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['descripcion']->renderLabel() ?></th>
        <td>
          <?php echo $form['descripcion']->renderError() ?>
          <?php echo $form['descripcion'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['seguimiento']->renderLabel() ?></th>
        <td>
          <?php echo $form['seguimiento']->renderError() ?>
          <?php echo $form['seguimiento'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['nivel_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['nivel_id']->renderError() ?>
          <?php echo $form['nivel_id'] ?>
        </td>
      </tr>
      <!--
      <tr>
        <th><?php echo $form['updated_at']->renderLabel() ?></th>
        <td>
          <?php echo $form['updated_at']->renderError() ?>
          <?php echo $form['updated_at'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['created_at']->renderLabel() ?></th>
        <td>
          <?php echo $form['created_at']->renderError() ?>
          <?php echo $form['created_at'] ?>
        </td>
      </tr>
    -->
    </tbody>
  </table>
</form>
