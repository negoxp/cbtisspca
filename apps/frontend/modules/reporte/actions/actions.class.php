<?php

/**
 * reporte actions.
 *
 * @package    netsales
 * @subpackage reporte
 * @author     Jorge Flores
 */
class reporteActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $user = $this->getUser();

    $c=new Criteria();
    if (!$user->isSuperAdmin()){
      //$c->addand(ReportePeer::USER_ID,$user->getProfile()->getUserId(), Criteria::EQUAL);
    }

    $this->reportes = ReportePeer::doSelect($c);
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new reporteForm();
    //$this->form->setAlumnoId($request->getParameter('alumno_id')); 
    $this->form->setDefault("user_id",$this->getUser()->getGuardUser()->getId());
    $this->form->setDefault("alumno_id",$request->getParameter('alumno_id'));
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new reporteForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($reporte = ReportePeer::retrieveByPk($request->getParameter('id')), sprintf('Object reporte does not exist (%s).', $request->getParameter('id')));
    $this->form = new reporteForm($reporte);
    $this->userid=$this->getUser()->getGuardUser()->getId();
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($reporte = ReportePeer::retrieveByPk($request->getParameter('id')), sprintf('Object reporte does not exist (%s).', $request->getParameter('id')));
    $this->form = new reporteForm($reporte);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($reporte = ReportePeer::retrieveByPk($request->getParameter('id')), sprintf('Object reporte does not exist (%s).', $request->getParameter('id')));
    $reporte->delete();

    $this->redirect('reporte/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $reporte = $form->save();

      $this->redirect('reporte/edit?id='.$reporte->getId());
    }
  }
}
