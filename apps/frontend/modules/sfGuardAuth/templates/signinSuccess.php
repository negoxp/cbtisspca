<form class="form-signin well" action="<?php echo url_for('@sf_guard_signin') ?>" method="post">
    <?php echo $form->renderHiddenFields(false) ?>
    <?php echo $form->renderGlobalErrors() ?>

    Username
    <input name="signin[username]" type="username" class="form-control" placeholder="Username">
    <?php echo $form['username']->renderError() ?>


    <br />
    Password
    <input name="signin[password]" type="password" class="form-control" placeholder="Password">
    <?php echo $form['password']->renderError() ?>
    <br />
    <button class="btn btn-lg btn-default btn-block" type="submit">Signin</button>


</form>
