<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<form action="<?php echo url_for('nivel/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <table class="table">
    <tfoot>
      <tr>
        <td colspan="2">
          <?php echo $form->renderHiddenFields(false) ?>
          &nbsp;<a href="<?php echo url_for('nivel/index') ?>"  class="btn btn-info">Regresar</a>
          <?php if (!$form->getObject()->isNew()): ?>
            &nbsp;<?php echo link_to('Eliminar', 'nivel/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => 'esta seguro?', 'class'=>"btn btn-danger")) ?>
          <?php endif; ?>
          <input type="submit" value="Guardar"  class="btn btn-default" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><?php echo $form['tipo_reporte']->renderLabel() ?></th>
        <td>
          <?php echo $form['tipo_reporte']->renderError() ?>
          <?php echo $form['tipo_reporte'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['nivel']->renderLabel() ?></th>
        <td>
          <?php echo $form['nivel']->renderError() ?>
          <?php echo $form['nivel']; ?>
        </td>
      </tr>
      <!--
      <tr>
        <th><?php echo $form['created_at']->renderLabel() ?></th>
        <td>
          <?php echo $form['created_at']->renderError() ?>
          <?php echo $form['created_at'] ?>
        </td>
      </tr>
    -->
    </tbody>
  </table>
</form>
