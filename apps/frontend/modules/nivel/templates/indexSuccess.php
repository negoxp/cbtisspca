<h1>Niveles</h1>

<table class="table table-striped">
  <thead>
    <tr>
      <th>Id</th>
      <th>Tipo reporte</th>
      <th>Ponderacion</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($nivels as $nivel): ?>
    <tr>
      <td><a href="<?php echo url_for('nivel/edit?id='.$nivel->getId()) ?>"><?php echo $nivel->getId() ?></a></td>
      <td><?php echo $nivel->getTipoReporte() ?></td>
      <td><?php echo $nivel->getNivel() ?></td>
      <td><i class="fa fa-circle" style="color:<?php echo colort($nivel->getNivel()); ?>;"></i></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

  <a href="<?php echo url_for('nivel/new') ?>" class="btn btn-default"><i class="fa fa-plus"></i> Agregar</a>
