<?php

/**
 * nivel actions.
 *
 * @package    netsales
 * @subpackage nivel
 * @author     Jorge Flores
 */
class nivelActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $this->nivels = NivelPeer::doSelect(new Criteria());
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new nivelForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new nivelForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($nivel = NivelPeer::retrieveByPk($request->getParameter('id')), sprintf('Object nivel does not exist (%s).', $request->getParameter('id')));
    $this->form = new nivelForm($nivel);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($nivel = NivelPeer::retrieveByPk($request->getParameter('id')), sprintf('Object nivel does not exist (%s).', $request->getParameter('id')));
    $this->form = new nivelForm($nivel);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($nivel = NivelPeer::retrieveByPk($request->getParameter('id')), sprintf('Object nivel does not exist (%s).', $request->getParameter('id')));
    $nivel->delete();

    $this->redirect('nivel/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $nivel = $form->save();

      $this->redirect('nivel/edit?id='.$nivel->getId());
    }
  }
}
