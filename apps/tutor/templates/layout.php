<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<head>
      <meta content="width=device-width, initial-scale=1" name="viewport">
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico" />

      <!-- Bootstrap core CSS -->
      <link rel="stylesheet" href="<?php echo public_path("vendor/theme") ?>/css/bootstrap.css">
      <!-- Bootstrap theme -->
      <!--  <link rel="stylesheet" href="css/bootstrap-theme.min.css"> -->

      <!-- Custom styles for this template -->
      <link rel="stylesheet" href="<?php echo public_path("vendor/theme") ?>/css/theme.css">
      <link rel="stylesheet" href="<?php echo public_path("vendor/theme") ?>/css/dashboard.css">
      <link rel="stylesheet" href="<?php echo public_path("vendor/theme") ?>/css/style.css">
      <link rel="stylesheet" href="<?php echo public_path("vendor/theme") ?>/css/dripicon.css">
      <link rel="stylesheet" href="<?php echo public_path("vendor/theme") ?>/css/typicons.css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="<?php echo public_path("vendor/theme") ?>/css/responsive.css">
      <link rel="stylesheet" href="<?php echo public_path("vendor/theme") ?>/js/tip/tooltipster.css">
      <link rel="stylesheet" type="text/css" href="<?php echo public_path("vendor/theme") ?>/js/vegas/jquery.vegas.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo public_path("vendor/theme") ?>/js/number-progress-bar/number-pb.css">
      <!-- pace loader -->
      <script src="<?php echo public_path("vendor/theme") ?>/js/pace/pace.js"></script>
      <link href="<?php echo public_path("vendor/theme") ?>/js/pace/themes/orange/pace-theme-flash.css" rel="stylesheet" />

      <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular.min.js"> </script>

      <script>
      <?php 
        $url= str_replace("request_password", "", url_for(""));
        $url= str_replace("sf_guard_user", "",$url);
      ?>
      var url="<?php echo $_SERVER['REQUEST_URI']?>";
      var url_general="<?php echo str_replace("sf_guard_permission", "", url_for(""));?>";

    </script>
      


      


      <?php include_stylesheets() ?>


      <!-- Main jQuery Plugins -->
      <script type='text/javascript' src="<?php echo public_path("vendor/theme") ?>/js/jquery.js"></script>

    <?php include_javascripts() ?>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script> 
  </head>
  <body role="document">

  <div id="preloader">
      <div id="status">&nbsp;</div>
  </div>



  <!-- Comtainer -->
  <div class="container-fluid paper-wrap bevel tlbr">
  <div id="paper-top">
  <div class="row">
  <div class="col-sm-3 no-pad">

      <a class="navbar-brand logo-text" href="#">SPCA CBTIS 19</a>

  </div>
&nbsp;&nbsp;&nbsp;
  <div class="col-sm-6 no-pad">


  </div>



  <div class="col-sm-3 no-pad">
  
  </div>
  </div>








  <!-- CONTENT -->
  <div class="wrap-fluid" id="paper-bg">
    <div class="tab-content">
    <?php if ($sf_request->getParameter('error')!=""){?>
    <?php if ($sf_request->getParameter('error')=="1"){?> 
    <div class="alert alert-danger" role="alert">
      <strong>Mensaje con ERROR!</strong> <?php echo $sf_request->getParameter('errorlog'); ?>
    </div>
    <?php }else{?>

    <div class="alert alert-success" role="alert">
      <strong>Mensaje Procesado!</strong> <?php echo $sf_request->getParameter('errorlog'); ?>
    </div>
    <?php 
    }
    }?>

  <?php if ($sf_user->hasFlash('error')): ?>
  <div class="alert alert-danger" role="alert"><?php echo $sf_user->getFlash('error') ?></div>
  <?php endif ?>
    

      <?php echo $sf_content ?>
    </div>
  </div>
  <!-- #/paper bg -->
  </div>
  <!-- ./wrap-sidebar-content -->

  <!-- / END OF CONTENT -->

  <!-- FOOTER -->
  <div id="footer">
      <div class="devider-footer-left"></div>
      <div class="time">
          <p id="spanDate"></p>
          <p id="clock"></p>
      </div>
      <div class="copyright">Copyright &copy; 2015 - CBTis19.mx
      </div>
      <div class="devider-footer"></div>
      <ul>
          <li><i class="fa fa-facebook-square"></i>
          </li>
          <li><i class="fa fa-twitter-square"></i>
          </li>
          <li><i class="fa fa-instagram"></i>
          </li>
      </ul>
  </div>
  <!-- / FOOTER -->
  </div>
  <!-- Container -->










    <script type='text/javascript' src='<?php echo public_path("vendor/theme") ?>/js/bootstrap.js'></script>
    <script type='text/javascript' src='<?php echo public_path("vendor/theme") ?>/js/date.js'></script>
    <script type='text/javascript' src='<?php echo public_path("vendor/theme") ?>/js/slimscroll/jquery.slimscroll.js'></script>
    <script type='text/javascript' src='<?php echo public_path("vendor/theme") ?>/js/jquery.nicescroll.min.js'></script>
    <script type='text/javascript' src='<?php echo public_path("vendor/theme") ?>/js/sliding-menu.js'></script>
    <script type='text/javascript' src='<?php echo public_path("vendor/theme") ?>/js/scriptbreaker-multiple-accordion-1.js'></script>
    <script type='text/javascript' src='<?php echo public_path("vendor/theme") ?>/js/tip/jquery.tooltipster.min.js'></script>
    <!--
    <script type='text/javascript' src="<?php echo public_path("vendor/theme") ?>/js/donut-chart/jquery.drawDoughnutChart.js"></script>
  -->
    <script type='text/javascript' src="<?php echo public_path("vendor/theme") ?>/js/tab/jquery.newsTicker.js"></script>
    <script type='text/javascript' src="<?php echo public_path("vendor/theme") ?>/js/tab/app.ticker.js"></script>
    <script type='text/javascript' src='<?php echo public_path("vendor/theme") ?>/js/app.js'></script>


    <script type='text/javascript' src='<?php echo public_path("vendor/theme") ?>/js/vegas/jquery.vegas.js'></script>
    <script type='text/javascript' src='<?php echo public_path("vendor/theme") ?>/js/image-background.js'></script>
    <script type="text/javascript" src="<?php echo public_path("vendor/theme") ?>/js/jquery.tabSlideOut.v1.3.js"></script>
    <script type="text/javascript" src="<?php echo public_path("vendor/theme") ?>/js/bg-changer.js"></script>

    <script type='text/javascript' src="<?php echo public_path("vendor/theme") ?>/js/number-progress-bar/jquery.velocity.min.js"></script>
    <script type='text/javascript' src="<?php echo public_path("vendor/theme") ?>/js/number-progress-bar/number-pb.js"></script>
    <script src="<?php echo public_path("vendor/theme") ?>/js/loader/loader.js" type="text/javascript"></script>
    <script src="<?php echo public_path("vendor/theme") ?>/js/loader/demo.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo public_path("vendor/theme") ?>/js/skycons/skycons.js"></script>

    <!-- FLOT CHARTS -->
    <script src="<?php echo public_path("vendor/theme") ?>/js/flot/jquery.flot.min.js" type="text/javascript"></script>
    <!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
    <script src="<?php echo public_path("vendor/theme") ?>/js/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
    <!-- FLOT PIE PLUGIN - also used to draw donut charts -->
    <script src="<?php echo public_path("vendor/theme") ?>/js/flot/jquery.flot.pie.min.js" type="text/javascript"></script>
    <!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
    <script src="<?php echo public_path("vendor/theme") ?>/js/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
    <!-- Page script -->

    <script src="<?php echo public_path("js") ?>/app.angular.js"></script>


  </body>
</html>
