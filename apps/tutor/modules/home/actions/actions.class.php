<?php

/**
 * home actions.
 *
 * @package    netsales
 * @subpackage home
 * @author     Jorge Flores
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class homeActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $errors = array();  // array to hold validation errors
    $data = array();        // array to pass back data

    //$request->checkCSRFProtection();

    //$this->forward('default', 'module');
    $consulta = $request->getParameter('consulta');
    $ncontrol= $consulta["ncontrol"];

    //Paso 1 Verificar si existe el numero de control 
    $c=new Criteria();
    $c->addand(AlumnoPeer::NCONTROL,$ncontrol, Criteria::EQUAL);
    $alumno=AlumnoPeer::doSelectOne($c);

    if (AlumnoPeer::doCount($c)!=1){
      $errors['name'] = 'No se encontro el numero de control del alumno';
    }

    if ( ! empty($errors)) {
      $this->getUser()->setFlash('error', 'No se encontro el numero de control del alumno', false);
      $this->forward('home', 'consulta');
      
      //return $response;

    } else {
      $this->alumno=$alumno;
      //verificar si existe un registro previo
      $c=new Criteria();
      $c->addand(ConsultaPeer::NCONTROL,$ncontrol, Criteria::EQUAL);
      if (ConsultaPeer::doCount($c)>=1){
        $consulta=ConsultaPeer::doSelectOne($c);
        $this->redirect('home/view?id='.$consulta->getId());

        //$this->form = new consultaForm($consulta);
      }else{
        $this->form = new consultaForm();
      }
      $this->form->setDefault('ncontrol',$ncontrol);
    }

    
  }


  public function executeConsulta(sfWebRequest $request)
  {
    //$this->forward('default', 'module');
    //$this->form = new consultaForm();
  }

  public function executeView(sfWebRequest $request)
  {
    $consulta = ConsultaPeer::retrieveByPk($request->getParameter('id'));
    $c=new Criteria();
    $c->addand(AlumnoPeer::NCONTROL,$consulta->getNcontrol(), Criteria::EQUAL);
    $alumno=AlumnoPeer::doSelectOne($c);
    $this->alumno=$alumno;

    $c=new Criteria();
    $c->addand(ReportePeer::ALUMNO_ID,$alumno->getId(), Criteria::EQUAL);
    $this->reportes=ReportePeer::doSelect($c);

    //Historial de hoy
    $c=new Criteria();
    $c->addand(CheckinPeer::ALUMNO_ID,$alumno->getId(), Criteria::EQUAL);
    $c->addand(CheckinPeer::TIEMPO, " DATE(tiempo)='".date('Y-m-d')."'", Criteria::CUSTOM);
    $c->addAscendingOrderByColumn(CheckinPeer::TIEMPO);
    $this->checkins_today=CheckinPeer::doSelect($c);

    //Historial de la semana 
    $c=new Criteria();
    $c->addand(CheckinPeer::ALUMNO_ID,$alumno->getId(), Criteria::EQUAL);
    $c->addand(CheckinPeer::TIEMPO, " DATE(tiempo)<='".date('Y-m-d')."'", Criteria::CUSTOM);
    $c->addand(CheckinPeer::TIEMPO, " DATE(tiempo)>='".date('Y-m-d', strtotime('Monday this week'))."'", Criteria::CUSTOM);
    $c->addAscendingOrderByColumn(CheckinPeer::TIEMPO);
    $this->checkins_week=CheckinPeer::doSelect($c);



  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    //$request->checkCSRFProtection();

    $consulta = $request->getParameter('consulta');
    $ncontrol= $consulta["ncontrol"];

    //Paso 1 Verificar si existe el numero de control 
    $c=new Criteria();
    $c->addand(AlumnoPeer::NCONTROL,$ncontrol, Criteria::EQUAL);
    $alumno=AlumnoPeer::doSelectOne($c);

    if (AlumnoPeer::doCount($c)!=1){
      $errors['name'] = 'No se encontro el numero de control del alumno';
    }

    if ( ! empty($errors)) {
      $this->getUser()->setFlash('error', 'No se encontro el numero de control del alumno', false);
      $this->forward('home', 'consulta');
      
      //return $response;

    } else {
      $this->alumno=$alumno;
      //verificar si existe un registro previo
      $c=new Criteria();
      $c->addand(ConsultaPeer::NCONTROL,$ncontrol, Criteria::EQUAL);
      if (ConsultaPeer::doCount($c)>=1){
        $consulta=ConsultaPeer::doSelectOne($c);
        $this->form = new consultaForm($consulta);
      }else{
        $this->form = new consultaForm();
      }
      $this->form->setDefault('ncontrol',$ncontrol);


      $this->processForm($request, $this->form);
      $this->setTemplate('index');
      
    }

    
  }

 public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($consulta = ConsultaPeer::retrieveByPk($request->getParameter('id')), sprintf('Object alumno does not exist (%s).', $request->getParameter('id')));
    $this->form = new consultaForm($consulta);


    $consulta = $request->getParameter('consulta');
    $ncontrol= $consulta["ncontrol"];

    //Paso 1 Verificar si existe el numero de control 
    $c=new Criteria();
    $c->addand(AlumnoPeer::NCONTROL,$ncontrol, Criteria::EQUAL);
    $alumno=AlumnoPeer::doSelectOne($c);

    if (AlumnoPeer::doCount($c)!=1){
      $errors['name'] = 'No se encontro el numero de control del alumno';
    }

    if ( ! empty($errors)) {
      $this->getUser()->setFlash('error', 'No se encontro el numero de control del alumno', false);
      $this->forward('home', 'consulta');
      
      //return $response;

    } else {
      $this->alumno=$alumno;
      //verificar si existe un registro previo
      $c=new Criteria();
      $c->addand(ConsultaPeer::NCONTROL,$ncontrol, Criteria::EQUAL);
      if (ConsultaPeer::doCount($c)>=1){
        $consulta=ConsultaPeer::doSelectOne($c);
        $this->form = new consultaForm($consulta);
      }else{
        $this->form = new consultaForm();
      }
      $this->form->setDefault('ncontrol',$ncontrol);


      $this->processForm($request, $this->form);
      $this->setTemplate('index');
      
    }

  }



  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $consulta = $form->save();

      $this->redirect('home/view?id='.$consulta->getId());
    }
  }
}
