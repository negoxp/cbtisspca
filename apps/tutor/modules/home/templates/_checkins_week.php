<?php
function spanishDate($diasemana)
{
   $diassemanaN= array("","Domingo","Lunes","Martes","Miércoles",
                  "Jueves","Viernes","Sábado");
   return $diassemanaN[$diasemana];
}  
?>

<table class="table table-striped">
  <thead>
    <tr>
			<th>Dia</th>
      <th>Entrada</th>
      <th>Salida</th>
    </tr>
  </thead>
  <tbody>
    <?php for ($i = 0; $i <= 10; $i++) { ?>
    <?php if (isset($checkins{$i}) ) { ?>
	    <tr>
	      <td><?php echo spanishDate($checkins{$i}->getDia()); ?></td>
	      <td><?= ( isset($checkins{$i}) ) ? date('Y-m-d H:i:s', strtotime($checkins{$i}->getTiempo())) : "" ?></td>
	      <?php 
	      if ( isset($checkins{$i+1}) ){
	      		if  (   $checkins{$i}->getDiaSemana() == $checkins{$i+1}->getDiaSemana() and  $checkins{$i+1}->getTipo()=="Salida"   ) {
			      	$i++;
			      ?>
			      	<td><?= ( isset($checkins{$i}) ) ? date('Y-m-d H:i:s', strtotime($checkins{$i}->getTiempo()))  : "" ?></td>
			      <?php
			      }else{
			      	echo "<td></td>";
			      }

	      }else{
					echo "<td></td>";
	    	}
	    ?> 	
	      
	    </tr>
    	<?php
    	}
  	} ?>
  </tbody>
</table>