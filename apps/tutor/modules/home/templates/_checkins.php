<table class="table table-striped">
  <thead>
    <tr>
      <th>Id</th>
      <th>Hora</th>
      <th>Tipo</th>
      <th>Dia Semana</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($checkins as $checkin): ?>
    <tr>
      <td><?php echo $checkin->getId() ?></td>
      <td><?php echo $checkin->getHora() ?></td>
      <td><?php echo $checkin->getTipo() ?></td>
      <td><?php echo $checkin->getDiaSemana() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>