<div class="hero-unit">
<h3>Consulta Alumnos</h3>
<p>
	Proporciona tus datos generales para poder consultar en el sistema de reportes.
</p>

<form action="<?php echo url_for('home/index') ?>" method="post">
  <table class="table" style="width:500px;">
    <tfoot>
      <tr>
        <td colspan="2">
          <input type="submit" value="Consultar" class="btn btn-success" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <tr>
        <th>* Número de control</th>
        <td>
          <input class="form-control form-cascade-control" type="text" name="consulta[ncontrol]" id="consulta_ncontrol">        </td>
      </tr>
    </tbody>
  </table>
</form>


</div>