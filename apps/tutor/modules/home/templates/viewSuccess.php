<div class="pull-right">
  <a href="<?php echo url_for('home/consulta') ?>" class="btn btn-info btn-lg" role="button">Salir</a>
</div>

 <table class="table" style="width:600px;">
  <tbody>
    <tr>
      <td style="width:200px;" >
        <?php if(is_file(sfConfig::get("sf_upload_dir").DIRECTORY_SEPARATOR.$alumno->getFoto())){ ?> 
        <img src="<?php echo public_path("uploads/".$alumno->getFoto(),1) ?>" width="150" /></td>
        <?php } ?>
      </td>
      <td>
        Control: <?php echo $alumno->getNcontrol(); ?>
        <br/>
        Nombre: <?php echo $alumno->getNombre()." ".$alumno->getPaterno()." ".$alumno->getMaterno(); ?>
        <br/>
        Carrera: <?php echo $alumno->getCarrera(); ?>
        <br/>
        Generacion: <?php echo $alumno->getGeneracion(); ?> / <?php echo $alumno->getTurno(); ?>
        <br/>
        Grado y grupo: <?php echo $alumno->getGrupo(); ?>
        <br/>
        Curp: <?php echo $alumno->getCurp(); ?>
        <br/>

        <br/>

        <table class="table">
          <tr>
            <th>Hoy</th>
            <th>Entrada</th>
            <th>Salida</th>
          </tr>
          <tr>
            <td><?php echo date('Y-m-d'); ?></td>
            <td><?= ( isset($checkins_today{0}) ) ? date('H:i:s', strtotime($checkins_today{0}->getTiempo())) : "" ?></td>
            <td><?= ( isset($checkins_today{1}) ) ? date('H:i:s', strtotime($checkins_today{1}->getTiempo()))  : "" ?></td>
          </tr>
        </table>
      </td>
    </tr>

  </tbody>
</table>

<div class="row">
  <div class="col-md-6"> 
    <?php include_partial('checkins_week', array('checkins' => $checkins_week)) ?>
  </div>
  <div class="col-md-6"> 
    <?php include_partial('reportes', array('reportes' => $reportes)) ?>
  </div>
</div>