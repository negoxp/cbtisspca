<table class="table table-striped" style="width:100%;">
  <thead>
    <tr>
      <th>Id</th>
      <th>Reporte</th>
      <th style="width:400px;">Descripcion</th>
      <th>Nivel</th>
      <th>Usuario</th>
      <th>Fecha</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($reportes as $reporte): ?>
    <tr>
      <td><?php echo $reporte->getId() ?></td>
      <td><?php echo $reporte->getReporte() ?></td>
      <td><?php echo $reporte->getDescripcion() ?></td>
      <td><?php echo $reporte->getNivelId() ?></td>
      <td><?php echo $reporte->getSfGuardUser()->getUsername(); ?></td>
      <td><?php echo $reporte->getCreatedAt() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>