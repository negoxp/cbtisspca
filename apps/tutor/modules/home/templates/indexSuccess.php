<div class="hero-unit">
<div class="pull-right">
  <a href="<?php echo url_for('home/consulta') ?>" class="btn btn-info btn-lg" role="button">Salir</a>
</div>

<h3>Consulta R&aacute;pida</h3>
<p>
	Proporciona tus datos generales para poder consultar en el sistema.
</p>

<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<form action="<?php echo url_for('home/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />

<?php endif; ?>



  <table class="table" style="width:500px;">
    <tfoot>
      <tr>
        <td colspan="2">
          <input type="submit" value="Consultar" class="btn btn-success" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <tr>
        <td colspan="2">     

      <div class="row" id="alumno_detail">
        <div class="col-md-4">
          <?php if(is_file(sfConfig::get("sf_upload_dir").DIRECTORY_SEPARATOR.$alumno->getFoto())){ ?> 
          <img src="<?php echo public_path("uploads/".$alumno->getFoto(),1) ?>" width="150" />
          <?php } ?>
        </div>
        <div class="col-md-8">
          <h3><?php echo $alumno->getNombreCompleto(); ?></h3>
          <br/>
          <h4><div ng-show="alumnoNumeroControl">Control:  <?php echo $alumno->getNcontrol(); ?></div></h4>
        </div>
      </div>


        </td>
      </tr>

      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th>*Quien consulta?</th>
        <td>
          <select>
            <option>Selecciona Opcion</option>
            <option>Papá</option>
            <option>Mamá</option>
            <option>Tutor</option>
          </select>
        </td>
      </tr>
      <tr>
        <th>*Nombre(s) Padre de familia</th>
        <td>
          <?php echo $form['nombres']->renderError() ?>
          <?php echo $form['nombres'] ?>
        </td>
      </tr>
      <tr>
        <th>*Apellidos Padre de familia</th>
        <td>
          <?php echo $form['apellidos']->renderError() ?>
          <?php echo $form['apellidos'] ?>
        </td>
      </tr>
      <tr>
        <th>*<?php echo $form['telefono']->renderLabel() ?></th>
        <td>
          <?php echo $form['telefono']->renderError() ?>
          <?php echo $form['telefono'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['celular']->renderLabel() ?></th>
        <td>
          <?php echo $form['celular']->renderError() ?>
          <?php echo $form['celular'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['email']->renderLabel() ?></th>
        <td>
          <?php echo $form['email']->renderError() ?>
          <?php echo $form['email'] ?>
        </td>
      </tr>


      <hr/>

      <tr class="hide">
        <th>Nombre(s) alumno</th>
        <td>
          <?php echo $form['nombre_alumno']->renderError() ?>
          <?php echo $form['nombre_alumno'] ?>
        </td>
      </tr>
      <tr class="hide">
        <th><?php echo $form['apellido_alumno']->renderLabel() ?></th>
        <td>
          <?php echo $form['apellido_alumno']->renderError() ?>
          <?php echo $form['apellido_alumno'] ?>
        </td>
      </tr>
      <tr class="hide">
        <th><?php echo $form['semestre']->renderLabel() ?></th>
        <td>
          <?php echo $form['semestre']->renderError() ?>
          <?php echo $form['semestre'] ?>
        </td>
      </tr>
      <tr class="hide">
        <th><?php echo $form['grupo']->renderLabel() ?></th>
        <td>
          <?php echo $form['grupo']->renderError() ?>
          <?php echo $form['grupo'] ?>
        </td>
      </tr>
      <tr class="hide">
        <th>* Número de control</th>
        <td>
          <?php echo $form['ncontrol']->renderError() ?>
          <?php echo $form['ncontrol'] ?>
        </td>
      </tr>
      <!--
      <tr>
        <th><?php echo $form['created_at']->renderLabel() ?></th>
        <td>
          <?php echo $form['created_at']->renderError() ?>
          <?php echo $form['created_at'] ?>
        </td>
      </tr>
  		-->
    </tbody>
  </table>
</form>


</div>