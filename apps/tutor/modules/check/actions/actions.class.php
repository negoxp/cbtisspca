<?php

/**
 * check actions.
 *
 * @package    netsales
 * @subpackage check
 * @author     Jorge Flores
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class checkActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    //$this->forward('default', 'module');
  }

  public function executeProcess(sfWebRequest $request)
  {
    $errors = array();  // array to hold validation errors
		$data = array();        // array to pass back data
  	
		// validate the variables ========
  	if ($request->getParameter('name')==""){
  		$errors['name'] = 'La lectura del código de barras es requerida. ';
  	}

  	//Buscar el alumno por el Numero de control
  	$c=new Criteria();
    $c->addand(AlumnoPeer::NCONTROL,$request->getParameter('name'), Criteria::EQUAL);
    $alumno=AlumnoPeer::doSelectOne($c); 

    if (AlumnoPeer::doCount($c)!=1){
    	$errors['name'] = 'No se encontro el numero de control del alumno';
   	}else{ 
	    
	    //Verificar si es la primera entrada del dia 
   		$c=new Criteria();
    	$c->addand(CheckinPeer::ALUMNO_ID,$alumno->getId(), Criteria::EQUAL);
    	$c->addand(CheckinPeer::TIEMPO, " DATE(tiempo)='".date('Y-m-d')."'", Criteria::CUSTOM);
    	

    	if (CheckinPeer::doCount($c)==0){
    		$check = new Checkin();
    		$check->setTipo('Entrada');
    	}else{
    		$c->addand(CheckinPeer::TIPO,'Salida', Criteria::EQUAL);
				if (CheckinPeer::doCount($c)>=1){
    			$check=CheckinPeer::doSelectOne($c); 
				}else{
					$check = new Checkin();
					$check->setTipo('Salida');
				}
    	}
 
	    $check->setAlumnoId($alumno->getId());
	    $check->setTiempo(date('Y-m-d H:i:s'));
	    
	    $check->setDiaSemana(date('l'));
	    $check->setDia(date('d'));
	    $check->setSemana(date('W'));
	    $check->setMes(date('m'));
	    $check->setAno(date('Y'));
	    $check->setHora(date('H:i:s'));

	    $check->setCreatedAt(date('Y-m-d H:i:s'));
	    $check->setUpdatedAt(date('Y-m-d H:i:s'));
	    $check->save();
  	}

    //echo public_path("uploads/".$alumno->getFoto(),1);
    //var_dump(sfConfig::get("sf_upload_dir"));
    //exit();

  	// response if there are errors
		if ( ! empty($errors)) {

		  // if there are items in our errors array, return those errors
		  $data['success'] = false;
		  $data['errors']  = $errors;
		} else {

		  // if there are no errors, return a message
		  $data['success'] = true;
		  $data['alumno_nombre'] = $alumno->getNombre()." ".$alumno->getPaterno()." ".$alumno->getMaterno();
		  $data['alumno_foto'] = $alumno->getFoto();
		  $data['alumno_ncontrol'] = $alumno->getNcontrol();
		  $data['alumno_checkin'] = date('H:i:s');
		  $data['message'] = 'Ingreso exitoso.';
		}

		// return all our data to an AJAX call
		echo json_encode($data);



		// return all our data to an AJAX call
		$this->setLayout(false);
		//return $this->renderText('No results.');
		exit();

  	//$this->setLayout(false);

  	//$this->returnJson($data);

  	


  }
}
