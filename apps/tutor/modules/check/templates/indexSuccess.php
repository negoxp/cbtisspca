<!-- SHOW ERROR/SUCCESS MESSAGES -->
<div ng-app="myApp" ng-controller="myCtrl">

<div id="success-alert" class="alert alert-success alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <div id="messages" ng-show="message">{{ message }}</div>
</div>

<div id="danger-alert" class="alert alert-danger alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <div id="messages" ng-show="message"><strong>{{ message }}</strong></div>
</div>



	
<div class="row">
	<div class="col-md-12">
		<form id="check_form" action="# "  onsubmit="return false;" ng-submit="processForm()" autocomplete="off">
		  <!-- NAME -->
		  <div id="name-group" class="form-group">

		    <input type="password" id="codebar_input" name="name" class="form-control" placeholder="Codigo de Barras" ng-model="formData.name" autocomplete="off" >
		    <span class="help-block"></span>
		    <span class="help-block" ng-show="errorName">{{ errorName }}</span> 
		  </div>

		<!-- SUBMIT BUTTON -->
		  <button type="submit" class="btn btn-success btn-lg btn-block">
		    <span class="glyphicon glyphicon-flash"></span> Enviar!
		  </button>
		</form>
	</div>
	<div class="col-md-12">
		<div class="row jumbotron" id="alumno_detail">
			<div class="col-md-3">
					<div id="publicpath" value="<?php echo public_path("uploads/",1) ?>" class="hide"></div>
          <div ng-init="alumnoFoto = ''">
				    <img ng-src="{{alumnoFoto}}"  width="100%" />
				</div>
			</div>
			<div class="col-md-9">
				<h1><div ng-show="alumnoNombre">{{ alumnoNombre }}</div></h1>
				<br/>
        <h2><div ng-show="alumnoNumeroControl">Control:  {{ alumnoNumeroControl }}</div></h2>

        <h1><div ng-show="alumnoCheckin" style="font-size: 103px;">  {{ alumnoCheckin }}</div></h1>
      
			</div>
		</div>
	
	</div>
</div>

<br/>
<br/>
	<ul id="listas">
	</ul>

</div>
