<?php

/**
 * Nivel form.
 *
 * @package    netsales
 * @subpackage form
 * @author     Jorge Flores
 */
class NivelForm extends BaseNivelForm
{
  public function configure()
  {
  	foreach ($this->getWidgetSchema()->getFields() as $field)
	{
	  $field->setAttribute('class', 'form-control form-cascade-control');
	}
  }

}
