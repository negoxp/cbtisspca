<?php

/**
 * Reporte form.
 *
 * @package    netsales
 * @subpackage form
 * @author     Jorge Flores
 */
class ReporteForm extends BaseReporteForm
{
  public function configure()
  {
  	foreach ($this->getWidgetSchema()->getFields() as $field)
	{
	  $field->setAttribute('class', 'form-control form-cascade-control');
	}

	$this->setWidget('descripcion', new sfWidgetFormTextarea(array('default' => 'Introduce aquí tu dirección'), array('cols' => 140, 'rows' => 5)));
  }


}
