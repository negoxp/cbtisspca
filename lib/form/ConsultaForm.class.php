<?php

/**
 * Consulta form.
 *
 * @package    netsales
 * @subpackage form
 * @author     Jorge Flores
 */
class ConsultaForm extends BaseConsultaForm
{
  public function configure()
  {
  	foreach ($this->getWidgetSchema()->getFields() as $field)
		{
		  $field->setAttribute('class', 'form-control form-cascade-control');
		}

		//$this->widgetSchema['ncontrol']->setAttribute('disabled', 'disabled');

  }
}
