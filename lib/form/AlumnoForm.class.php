<?php

/**
 * Alumno form.
 *
 * @package    netsales
 * @subpackage form
 * @author     Jorge Flores
 */
class AlumnoForm extends BaseAlumnoForm
{
  public function configure()
  {
  	foreach ($this->getWidgetSchema()->getFields() as $field)
	{
	  $field->setAttribute('class', 'form-control form-cascade-control');
	}

	$this->setWidget('foto', new sfWidgetFormInputFileEditable(
	        array(
	            'edit_mode'=>false,
	            'with_delete' => false,
	            'file_src' => '',
	        )
	      ));
	$this->setValidator('foto', new sfValidatorFile(
	        array(
	            'max_size' => 90000000,
	            'mime_types' => 'web_images', //you can set your own of course
	            'path' => sfConfig::get('sf_web_dir')."/uploads/",
	            'required' => false,
	            // 'validated_file_class' => 'sfValidatedFileCustom'
	        )
	 
	      ));
	
  }
}
