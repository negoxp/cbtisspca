<?php

/**
 * Project filter form base class.
 *
 * @package    netsales
 * @subpackage filter
 * @author     Jorge Flores
 */
abstract class BaseFormFilterPropel extends sfFormFilterPropel
{
  public function setup()
  {
  }
}
